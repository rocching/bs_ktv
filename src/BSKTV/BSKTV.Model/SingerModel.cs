﻿using Roc.Data;

namespace BSKTV.Model
{
    [Table("T_Singer")]
    public class SingerModel : IPrimaryKey
    {
        /// <summary>
        /// 唯一ID
        /// </summary>
        [Key(true)]
        public long Id { get; set; }

        /// <summary>
        /// 歌手ID
        /// </summary>
        public virtual string SingerId { get; set; }

        /// <summary>
        /// 歌手姓名
        /// </summary>
        public virtual string SingerName { get; set; }

        /// <summary>
        /// 姓名拼音
        /// </summary>
        public string SingerNameCode { get; set; }

        /// <summary>
        /// 首字母
        /// </summary>
        public string SingerNameSzm { get; set; }

        /// <summary>
        /// 类型 1=华语 2=欧美 3=日韩 4=其他 5=日本 6=韩国
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 性别类型 1=男 2=女 3=组合
        /// </summary>
        public string SexType { get; set; }

        /// <summary>
        /// 图片
        /// </summary>
        public string Pic { get; set; }

        /// <summary>
        /// 介绍
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 粉丝数量
        /// </summary>
        public int FansCount { get; set; }
    }
}
