﻿using System.Collections.Generic;

namespace BSKTV.Model
{
    public class MusicMvUrlModel
    {
        public string Url { get; set; }

        public string Hash { get; set; }

        public long Timelength { get; set; }

        public long FileSize { get; set; }

        public long Bitrate { get; set; }

        public List<string> UrlList { get; set; }

        public bool IsValid()
        {
            return !string.IsNullOrEmpty(Url);
        }

        public MusicMvUrlModel(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return;
            }

            Url = url;

            UrlList = new List<string>();
            UrlList.Add(url);
        }

        public MusicMvUrlModel()
        {

        }
    }
}
