﻿using Roc.Data;

namespace BSKTV.Model
{
    [Table("T_Dictionary")]
    public class DictionaryModel : IPrimaryKey
    {
        [Key(true)]
        public long Id { get; set; }

        public string Type { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public int SortId { get; set; }
    }
}
