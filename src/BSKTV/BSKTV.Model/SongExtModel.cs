﻿namespace BSKTV.Model
{
    public class SongExtModel
    {
        /// <summary>
        /// 截屏位置 单位秒
        /// </summary>
        public int ScreenshotPosition { get; set; }

        public SongExtModel()
        {
            ScreenshotPosition = 1;
        }
    }
}
