﻿namespace BSKTV.Model
{
    public class SongPlayListModel : SongModel
    {
        /// <summary>
        /// 播放ID T_PlayList 表ID
        /// </summary>
        public long PlayId { get; set; }

        /// <summary>
        /// 客户端ID
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// 点歌的人
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 排序字段
        /// </summary>
        public int SortId { get; set; }
    }
}
