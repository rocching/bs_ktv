﻿namespace BSKTV.Model
{
    public interface IPrimaryKey
    {
        long Id { get; set; }
    }
}
