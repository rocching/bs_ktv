﻿namespace BSKTV.Model
{
    public class Constant
    {
        public const string AppName = "BS_KTV";

        public const string AppVersion = "1.3.1";

        public const string AppText = "宝山点歌";

        public const int DEFAULT_MUSIC_MAX_VOLUME = 1000;

        public const int DEFAULT_MUSIC_MIN_VOLUME = 0;

        public const int DEFAULT_MUSIC_VOLUME_STEP = 5;

        public const int DEFAULT_SERVER_PORT = 9900;

        public const string DEFAULT_SERVER_ID = "BS_KTV_SERVER";

        public const string DEFAULT_SERVER_NAME = "管理员";

        public const string TOKEN_NOTIFY_ICON = "NotifyIconToken";

        public const string DICTIONARY_TYPE = "type";

        public const string DICTIONARY_TYPE_DEFAULT = "0";

        public const string SONG_SOURCE_MV91 = "91mv";

        public const int MV91_SONG_PAGE_SIZE = 10;

        public const string MV91_VIP_USER_ACCOUNT = "793676";

        public const string SINGER_SEX_TYPE = "singer_sex_type";

        public const string SINGER_SEX_TYPE_MALE = "1";

        public const string SINGER_SEX_TYPE_FEMALE = "0";

        public const string SINGER_SEX_TYPE_GROUP = "3";

        public const string SINGER_TYPE = "singer_type";

        public const string SINGER_TYPE_DEFAULT_VALUE = "0";

        public const string SINGER_SEX_TYPE_DEFAULT_VALUE = "0";

        public const string SINGER_FIELD_SINGER_NAME = "SingerName";

        public const string SONG_TYPE = "song_type";

        public const string MUSIC_LIST_NAME = "MusicList";

        public const int SONG_TYPE_DOWNLOADED = 2;

        public const int SONG_TYPE_DB = 1;

        public const int SONG_TYPE_CLOUD = 3;

        public const int DEFAULT_PLAY_LIST_SORTID = 1000;

        /// <summary>
        /// 处理图片
        /// </summary>
        public const int SONG_HANDLE_TYPE_PIC = 1;

        /// <summary>
        /// 处理播放URL
        /// </summary>
        public const int SONG_HANDLE_TYPE_URL = 2;

        public class KtvServer
        {
            public const string DEFAULT_PREFIX = "KTV";

            public const string END_CHAR = "$";

            public const string CMD_SPLIT_CHAR = "#";

            public const string PARAMETER_SPLIT_CHAR = "#";
        }

        public class DateTimeFormat
        {
            public const string FORMAT_1 = "yyyy-MM-dd HH:mm:ss";
        }

        public class Task
        {
            public const string MUSIC_URL_TASK = "MUSIC_URL_TASK";

            public const string MUSCI_PIC_TASK = "MUSCI_PIC_TASK";

            public const string MUSIC_PIC_CHECK_TASK = "MUSIC_PIC_CHECK_TASK";
        }

        public class Brush
        {
            public const string Home_Brush = nameof(Home_Brush);

            public const string Singer_Brush = nameof(Singer_Brush);
        }

        public class App
        {
            public const string HomePage = "HomePage";
            public const string DictionaryPage = "Dictionary.DictionaryListPage";
            public const string SingerPage = "Singer.SingerListPage";
            public const string KugouSingerPage = "Singer.KgSingerListPage";

            public const string SongPage = "Song.SongListPage";
            public const string Net91SongPage = "Song.Net91SongListPage";
            public const string PlayListPage = "Song.PlayListPage";

            public const string ServerPage = "Server.ServerPage";
        }

        public class MessageToken
        {
            public const string LoadContent = nameof(LoadContent);
        }
    }
}
