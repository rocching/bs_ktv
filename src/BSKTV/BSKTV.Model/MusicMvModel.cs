﻿using System.Collections.Generic;
using System.Linq;

namespace BSKTV.Model
{
    public class MusicMvModel
    {
        public int Id { get; set; }

        public string SongName { get; set; }

        public string Hash { get; set; }

        public string PublishDate { get; set; }

        public string MvIcon { get; set; }

        public int AuthorId { get; set; }

        public string AuthorName { get; set; }

        public string AuthorAvatar { get; set; }

        public int LikeCount { get; set; }

        public int CollectCount { get; set; }

        public int CommentCount { get; set; }

        public List<MusicMvUrlModel> UrlList { get; set; }

        public string GetPlayUrl()
        {
            var list = UrlList;
            if (list != null && list.Count > 0)
            {
                var model = list.FirstOrDefault();
                if (model != null)
                {
                    return model.Url;
                }
            }
            return string.Empty;
        }

        //public string GetPlayUrl()
        //{
        //    var list = UrlList;

        //    if (list != null && list.Count > 0)
        //    {
        //        var model = list.OrderByDescending(m => m.FileSize).FirstOrDefault();
        //        foreach (var item in model.UrlList)
        //        {
        //            if (item.Contains("ali.kugou.com"))
        //            {
        //                return item;
        //            }
        //        }
        //        return model.Url;
        //    }

        //    return string.Empty;
        //}
    }
}
