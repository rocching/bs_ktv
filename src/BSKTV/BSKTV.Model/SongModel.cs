﻿using BSKTV.Util;
using Roc.Data;

namespace BSKTV.Model
{
    [Table("T_Song")]
    public class SongModel : IPrimaryKey
    {
        /// <summary>
        /// 唯一ID
        /// </summary>
        [Key(true)]
        public long Id { get; set; }

        /// <summary>
        /// 来源
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// 类型 1=已入库 2=已下载 3=云端
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 歌曲ID
        /// </summary>
        public string SongId { get; set; }

        /// <summary>
        /// 歌曲名称
        /// </summary>
        public string SongName { get; set; }

        /// <summary>
        /// 歌曲名称中文拼音
        /// </summary>
        public string SongNameCode { get; set; }

        /// <summary>
        /// 歌曲名称首字母
        /// </summary>
        public string SongNameSzm { get; set; }

        /// <summary>
        /// 歌手
        /// </summary>
        public string SingerName { get; set; }

        /// <summary>
        /// 本地地址
        /// </summary>
        public string LocalPath { get; set; }

        /// <summary>
        /// 图片
        /// </summary>
        public string Pic { get; set; }

        /// <summary>
        /// 播放地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// mv播放地址
        /// </summary>
        public string MvUrl { get; set; }

        /// <summary>
        /// 歌词
        /// </summary>
        public string Lrc { get; set; }

        /// <summary>
        /// mv hash
        /// </summary>
        public string MvHash { get; set; }

        /// <summary>
        /// 扩展信息
        /// </summary>
        public string ExtData { get; set; }

        public SongExtModel GetExtInfo()
        {
            if (string.IsNullOrEmpty(ExtData))
            {
                return new SongExtModel();
            }

            return StringUtil.Parse<SongExtModel>(ExtData);
        }

        public static string GetSourceName(string type)
        {
            string name = "";
            switch (type)
            {
                case Constant.SONG_SOURCE_MV91:
                    name = "91MV";
                    break;
                default:
                    name = "未知";
                    break;
            }
            return name;
        }

        public static string GetTypeName(int type)
        {
            string name = "";
            switch (type)
            {
                case 1:
                    name = "已入库";
                    break;
                case 2:
                    name = "已下载";
                    break;
                case 3:
                    name = "云端";
                    break;
                default:
                    break;
            }
            return name;
        }

        public override string ToString()
        {
            return $"[{GetSourceName(Source)}][{SingerName}]{SongName}";
        }
    }
}
