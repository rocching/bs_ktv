﻿using Roc.Data;

namespace BSKTV.Model
{
    [Table("T_PlayList")]
    public class PlayListModel : IPrimaryKey
    {
        /// <summary>
        /// 唯一ID
        /// </summary>
        [Key(true)]
        public long Id { get; set; }

        public long SongId { get; set; }

        public string ClientId { get; set; }

        public string UserName { get; set; }

        public int SortId { get; set; }
    }
}
