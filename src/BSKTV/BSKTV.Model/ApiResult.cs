﻿namespace BSKTV.Model
{
    public class ApiResult<T>
    {
        public bool Flag { get; set; }

        public string Message { get; set; }

        public T Data { get; set; }

        public ApiResult(bool flag, string message, T data = default)
        {
            Flag = flag;
            Message = message;
            Data = data;
        }

        public static ApiResult<T> Success()
        {
            return new ApiResult<T>(true, "");
        }

        public static ApiResult<T> Success(T data, string message = "")
        {
            return new ApiResult<T>(true, message, data);
        }

        public static ApiResult<T> Fail()
        {
            return new ApiResult<T>(false, "");
        }

        public static ApiResult<T> Fail(string message, T data = default)
        {
            return new ApiResult<T>(false, message, data);
        }

        public static ApiResult<T> Result(bool flag, string message, T data = default)
        {
            return new ApiResult<T>(flag, message, data);
        }
    }

    public class ApiResult : ApiResult<object>
    {
        public ApiResult(bool flag, string message, object data = null) : base(flag, message, data)
        {

        }

        public static new ApiResult Result(bool flag, string message, object data = null)
        {
            return new ApiResult(flag, message, data);
        }

        public static new ApiResult Success()
        {
            return new ApiResult(true, "");
        }

        public static new ApiResult Fail()
        {
            return new ApiResult(false, "");
        }
    }

    public class ErrorResponse
    {
        public int Status { get; set; }

        public string Timestamp { get; set; }

        public string Message { get; set; }
    }
}
