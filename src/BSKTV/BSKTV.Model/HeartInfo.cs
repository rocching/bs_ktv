﻿using System;

namespace BSKTV.Model
{
    public class HeartInfo
    {
        public HeartInfo() { }

        public int Flag { get; set; }

        public string Now { get; set; }

        public static HeartInfo Success()
        {
            return new HeartInfo() { Flag = 1, Now = DateTime.Now.ToString(Constant.DateTimeFormat.FORMAT_1) };
        }
    }
}
