﻿namespace BSKTV.Model
{
    public class PageInput
    {
        /// <summary>
        /// 页码
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// 页面大小
        /// </summary>
        public int PageSize { get; set; }

        public PageInput()
        {
            PageIndex = 1;
            PageSize = 10;
        }

        public virtual void Check()
        {
            if (PageIndex < 1)
            {
                PageIndex = 1;
            }

            if (PageSize < 1)
            {
                PageSize = 10;
            }
        }
    }
}
