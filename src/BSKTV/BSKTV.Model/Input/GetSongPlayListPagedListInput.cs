﻿namespace BSKTV.Model.Input
{
    public class GetSongPlayListPagedListInput : GetSongPagedListInput
    {
        public string UserName { get; set; }

        public string ClientId { get; set; }
    }
}
