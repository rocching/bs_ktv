﻿namespace BSKTV.Model.Input
{
    public class GetSongPagedListInput : PageInput
    {
        public string Source { get; set; }

        public int Type { get; set; }

        public string Keyword { get; set; }
    }
}
