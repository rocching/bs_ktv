﻿namespace BSKTV.Model.Input
{
    public class GetDictionaryPagedListInput : PageInput
    {
        public string Type { get; set; }

        public string Keyword { get; set; }

        public bool HasType()
        {
            return !string.IsNullOrEmpty(Type) && Type != "0";
        }
    }
}
