﻿namespace BSKTV.Model.Input
{
    public class GetKgSingerListInput : PageInput
    {
        public string SexType { get; set; }

        public string Type { get; set; }

        public override void Check()
        {
            base.Check();

            if (string.IsNullOrEmpty(SexType))
            {
                SexType = "0";
            }

            if (string.IsNullOrEmpty(Type))
            {
                Type = "0";
            }
        }
    }
}
