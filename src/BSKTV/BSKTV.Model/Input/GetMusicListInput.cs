﻿namespace BSKTV.Model
{
    public class GetMusicListInput : PageInput
    {
        public string Type { get; set; }

        public string Input { get; set; }

        public string Filter { get; set; }
    }
}
