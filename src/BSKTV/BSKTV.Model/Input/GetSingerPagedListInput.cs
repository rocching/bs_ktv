﻿namespace BSKTV.Model.Input
{
    public class GetSingerPagedListInput : GetKgSingerListInput
    {
        /// <summary>
        /// 关键词
        /// </summary>
        public string Keyword { get; set; }
    }
}
