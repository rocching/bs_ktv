﻿namespace BSKTV.Model
{
    public class GetMusicImgListInput : PageInput
    {
        /// <summary>
        /// 关键词
        /// </summary>
        public string Keyword { get; set; }
    }
}
