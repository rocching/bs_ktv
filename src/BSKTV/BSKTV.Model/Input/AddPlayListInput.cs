﻿using System.Collections.Generic;
using System;

namespace BSKTV.Model.Input
{
    public class AddPlayListInput
    {
        public string ClientId { get; set; }

        public string UserName { get; set; }

        public List<long> SongIdList { get; set; }

        public void Check()
        {
            if (string.IsNullOrEmpty(ClientId))
            {
                throw new ArgumentException("客户端ID参数不能为空", nameof(ClientId));
            }

            if (string.IsNullOrEmpty(UserName))
            {
                throw new ArgumentException("用户名参数不能为空", nameof(UserName));
            }

            if (SongIdList == null || SongIdList.Count < 1)
            {
                throw new ArgumentException("点歌列表不能为空", nameof(SongIdList));
            }
        }
    }
}
