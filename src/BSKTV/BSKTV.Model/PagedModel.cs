﻿using System.Collections.Generic;

namespace BSKTV.Model
{
    public class PagedModel<T> : PageInput
    {
        public long TotalCount { get; set; }

        public List<T> List { get; set; }

        public PagedModel()
        {
            TotalCount = 0;
            List = new List<T>();
        }

        public PagedModel(PageInput input, long totalCount, List<T> List)
        {
            this.PageIndex = input.PageIndex;
            this.PageSize = input.PageSize;

            this.TotalCount = totalCount;
            this.List = List;
        }

        public long GetTotalPage()
        {
            return (TotalCount + PageSize - 1) / PageSize;
        }

        public bool HasData()
        {
            return TotalCount > 0 && List != null && List.Count > 0;
        }

        public static PagedModel<T> Empty()
        {
            return new PagedModel<T>();
        }
    }

    public class PagedModel : PagedModel<object>
    {

    }
}
