﻿using BSKTV.Model;
using BSKTV.Util;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BSKTV.Service
{
    public abstract class BaseService
    {
        private string baseUrl;
        public BaseService()
        {

        }

        public BaseService(string url)
        {
            baseUrl = url;
        }

        public string BaseUrl
        {
            get => BaseUrl;
            set => baseUrl = value;
        }

        #region 本地文件

        public virtual string GetFilePath(string fileName)
        {
            return FileUtil.GetFilePath($"Data\\{fileName}");
        }

        public virtual string[] ReadAllLines(string fileName)
        {
            string path = GetFilePath(fileName);

            return FileUtil.ReadAllLines(path);
        }

        public virtual T GetFileModel<T>(string fileName) where T : class, new()
        {
            string path = GetFilePath(fileName);
            string json = FileUtil.ReadAllText(path);
            if (string.IsNullOrEmpty(json))
            {
                return new T();
            }

            return StringUtil.Parse<T>(json);
        }

        public virtual void Update<T>(T t, string file_name) where T : class
        {
            if (t == null)
            {
                return;
            }

            string path = GetFilePath(file_name);
            FileUtil.WriteAllText(path, StringUtil.ToJson(t));
        }
        #endregion

        #region 网络
        public virtual async Task<ApiResult<T>> PostAsync<T>(string url, Dictionary<string, object> parameters = null, bool useBaseUrl = true, Action<HttpItem> action = null, Action<HttpResult> resultAction = null)
        {
            void SetHttpItem(HttpItem item)
            {
                item.Method = "POST";
                item.Accept = "*/*";

                item.ContentType = "application/x-www-form-urlencoded";
                item.PostEncoding = Encoding.UTF8;
                item.Postdata = GetParamString(parameters);
                action?.Invoke(item);
            };

            return await Task.Factory.StartNew(() =>
            {
                return Get<T>(url, null, useBaseUrl, SetHttpItem, resultAction);
            });
        }

        public virtual async Task<ApiResult<T>> GetAsync<T>(string url, Dictionary<string, object> parameters = null, bool useBaseUrl = true, Action<HttpItem> action = null, Action<HttpResult> resultAction = null)
        {
            return await Task.Factory.StartNew(() =>
            {
                return Get<T>(url, parameters, useBaseUrl, action, resultAction);
            });
        }

        public virtual ApiResult<T> Get<T>(string url, Dictionary<string, object> parameters = null, bool useBaseUrl = true, Action<HttpItem> action = null, Action<HttpResult> resultAction = null)
        {
            var result = GetString(url, parameters, useBaseUrl, action, resultAction);
            if (result.Flag)
            {
                return ApiResult<T>.Success(StringUtil.Parse<T>(result.Data));
            }
            return ApiResult<T>.Fail(result.Message);
        }

        public virtual async Task<ApiResult<string>> GetStringAsync(string url, Dictionary<string, object> parameters = null, bool useBaseUrl = true, Action<HttpItem> action = null, Action<HttpResult> resultAction = null)
        {
            return await Task.Factory.StartNew(() =>
            {
                return GetString(url, parameters, useBaseUrl, action, resultAction);
            });
        }

        public virtual ApiResult<string> GetString(string url, Dictionary<string, object> parameters = null, bool useBaseUrl = true, Action<HttpItem> action = null, Action<HttpResult> resultAction = null)
        {
            HttpItem item = new HttpItem
            {
                URL = GetUrl(url, parameters, useBaseUrl)
            };

            action?.Invoke(item);

            HttpResult result = HttpUtil.Instance.Request(item);
            if (result.Success)
            {
                resultAction?.Invoke(result);
                //LogUtil.Instance.WriteLog("请求结果:" + result.Html);
                return ApiResult<string>.Success(result.Html);
            }
            else
            {
                var res = StringUtil.Parse<ErrorResponse>(result.Html);
                string msg = "网络错误";
                if (res != null)
                {
                    msg = res.Message;
                }
                LogUtil.Instance.Error(msg);
                LogUtil.Instance.Error(result.Html);
                return ApiResult<string>.Fail(msg);
            }
        }

        public virtual async Task<ApiResult<string>> DownloadFileAsync(string url, string filePath, Dictionary<string, object> parameters = null, bool useBaseUrl = true, Action<double, double> downloadProcessing = null, Action<double> downlaodCompleted = null)
        {
            return await Task.Factory.StartNew(() => DownloadFile(url, filePath, parameters, useBaseUrl, downloadProcessing, downlaodCompleted));
        }

        public virtual ApiResult<string> DownloadFile(string url, string filePath, Dictionary<string, object> parameters = null, bool useBaseUrl = true, Action<double, double> downloadProcessing = null, Action<double> downlaodCompleted = null)
        {
            HttpItem item = new HttpItem
            {
                IsDownloadFile = true,
                ContentType = "application/octet-stream",
                Allowautoredirect = false,
                URL = GetUrl(url, parameters, useBaseUrl),
                SaveFilePath = filePath,
                DownloadProcessing = downloadProcessing,
                DownloadCompleted = downlaodCompleted
            };

            HttpResult result = HttpUtil.Instance.Request(item);
            if (result.Success)
            {
                return ApiResult<string>.Success("下载成功");
            }
            else
            {
                return ApiResult<string>.Fail("下载失败");
            }
        }

        private string GetUrl(string url, Dictionary<string, object> parameters, bool useBaseUrl = true)
        {
            StringBuilder sb = new StringBuilder();
            if (useBaseUrl && !string.IsNullOrEmpty(baseUrl))
            {
                sb.Append(baseUrl);
            }
            sb.Append(url);
            if (parameters != null && parameters.Count > 0)
            {
                if (!sb.ToString().Contains("?"))
                {
                    sb.Append("?");
                }
                else
                {
                    sb.Append("&");
                }

                int index = 0;
                foreach (var item in parameters)
                {
                    sb.AppendFormat("{0}={1}", item.Key, item.Value);
                    if (index < parameters.Count - 1)
                    {
                        sb.Append("&");
                    }
                    index++;
                }
            }
            return sb.ToString();
        }

        protected virtual string GetParamString(Dictionary<string, object> parameters)
        {
            StringBuilder sb = new StringBuilder();
            if (parameters != null && parameters.Count > 0)
            {
                int index = 0;
                foreach (var item in parameters)
                {
                    sb.AppendFormat("{0}={1}", item.Key, item.Value);
                    if (index < parameters.Count - 1)
                    {
                        sb.Append("&");
                    }
                    index++;
                }
            }
            return sb.ToString();
        }

        #endregion

        #region HTML
        protected string GetNodeAttrText(HtmlNode node, string xpath, string attrName)
        {
            var child = GetNode(node, xpath);
            if (child != null)
            {
                var attrs = child.Attributes;
                if (attrs != null)
                {
                    var attr = child.Attributes[attrName];
                    if (attr != null)
                    {
                        return attr.Value;
                    }
                }
            }
            return string.Empty;
        }

        protected string GetNodeAttrText(HtmlNode node, string attrName)
        {
            var attrs = node.Attributes;
            if (attrs != null)
            {
                var attr = node.Attributes[attrName];
                if (attr != null)
                {
                    return attr.Value;
                }
            }
            return string.Empty;
        }

        protected string GetNodeText(HtmlNode node, string xpath)
        {
            var child = GetNode(node, xpath);
            if (child != null)
            {
                return child.InnerText.Trim();
            }
            return string.Empty;
        }

        protected HtmlNode GetNode(HtmlNode node, string xpath)
        {
            return node.SelectSingleNode(xpath);
        }

        protected HtmlNodeCollection GetNodeList(HtmlNode node, string xpath)
        {
            return node.SelectNodes(xpath);
        }
        #endregion
    }
}
