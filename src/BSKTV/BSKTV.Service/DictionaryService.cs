﻿using BSKTV.Data;
using BSKTV.Model;
using BSKTV.Model.Input;
using BSKTV.Util;
using Roc.Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BSKTV.Service
{
    public class DictionaryService
    {
        private readonly DictionaryRepository dictionaryRepository;
        public DictionaryService(DictionaryRepository dictionaryRepository)
        {
            this.dictionaryRepository = dictionaryRepository;
        }

        public async Task<List<DictionaryModel>> GetList(string type)
        {
            return await dictionaryRepository.GetList(m => m.Type == type, m => m.SortId);
        }

        public async Task<PagedModel<DictionaryModel>> GetPagedList(GetDictionaryPagedListInput input)
        {
            input.Check();

            Expression<Func<DictionaryModel, bool>> where = ExpressionUtil.Default<DictionaryModel>();

            string type = input.Type;
            string keyword = input.Keyword;

            where = where.MergeAnd(input.HasType(), m => m.Type == type);

            where = where.MergeAnd(!string.IsNullOrEmpty(keyword), m => m.Code == keyword || m.Name.Contains(keyword));

            return await dictionaryRepository.GetPagedList(input, where, "Type,SortId");
        }

        public async Task<DictionaryModel> Save(DictionaryModel model)
        {
            DictionaryModel dicModel = null;

            if (model.Id > 0)
            {
                dicModel = await dictionaryRepository.Get(model.Id);
            }

            if (dicModel != null && dicModel.Id > 0)
            {
                dicModel.SortId = model.SortId;
                dicModel.Type = model.Type;
                dicModel.Code = model.Code;
                dicModel.Name = model.Name;

                await dictionaryRepository.UpdateAsync(dicModel, m => m.Id == dicModel.Id);
            }
            else
            {
                dicModel = StringUtil.Copy(model);

                int id = await dictionaryRepository.InsertWithIdAsync(dicModel);

                dicModel.Id = id;
            }

            return dicModel;
        }

        public async Task<bool> Delete(long id)
        {
            if (id < 1)
            {
                return false;
            }

            return await dictionaryRepository.DeleteAsync(m => m.Id == id);
        }

        public async Task<bool> Delete(List<long> idList)
        {
            if (StringUtil.IsEmpty(idList))
            {
                return false;
            }

            return await dictionaryRepository.DeleteAsync(m => SqlFunction.In(m.Id, idList));
        }
    }
}
