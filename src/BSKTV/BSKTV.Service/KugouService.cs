﻿using BSKTV.Model;
using BSKTV.Model.Input;
using BSKTV.Util;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSKTV.Service
{
    public class KugouService : BaseService
    {
        public KugouService()
        {

        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="input">查询实体</param>
        /// <returns></returns>
        public async Task<PagedModel<SingerModel>> GetPagedList(GetKgSingerListInput input)
        {
            input.Check();

            string url = $"http://mobilecdnbj.kugou.com/api/v5/singer/list?version=9108&showtype=1&plat=0&sextype={input.SexType}&sort=1&musician=0&type={input.Type}&pagesize={input.PageSize}&page={input.PageIndex}";

            var empty = PagedModel<SingerModel>.Empty();

            List<SingerModel> list = new List<SingerModel>();
            var result = await base.GetAsync<JObject>(url, null, false);
            if (result != null && result.Flag)
            {
                var d = result.Data;
                if (d == null)
                {
                    return empty;
                }

                int status = d.Value<int>("status");
                if (status != 1)
                {
                    return empty;
                }

                var data = d.Value<JObject>("data");
                if (data == null)
                {
                    return empty;
                }

                var array = data.Value<JArray>("info");
                if (array == null || array.Count < 1) return empty;

                var total = data.Value<long>("total");

                foreach (var item in array)
                {
                    int singerId = item.Value<int>("singerid");
                    string imgUrl = item.Value<string>("imgurl");
                    string singerName = item.Value<string>("singername");

                    if (string.IsNullOrEmpty(singerName))
                    {
                        continue;
                    }

                    string singerNameCode = WordUtil.GetPinyin(singerName);
                    string singerNameSzm = WordUtil.GetFirstPinyin(singerName);

                    if (!string.IsNullOrEmpty(imgUrl))
                    {
                        imgUrl = imgUrl.Replace("{size}", "240");
                    }

                    SingerModel model = new SingerModel
                    {
                        SingerId = singerId.ToString(),
                        SingerName = singerName,
                        Description = item.Value<string>("descibe"),
                        FansCount = item.Value<int>("fanscount"),
                        Pic = imgUrl,
                        Type = input.Type,
                        SexType = input.SexType,
                        SingerNameCode = singerNameCode,
                        SingerNameSzm = singerNameSzm
                    };

                    list.Add(model);
                }

                WordUtil.ClearCache();

                return new PagedModel<SingerModel>(input, total, list);
            }

            return empty;
        }
    }
}
