﻿using BSKTV.Data;
using Microsoft.Extensions.DependencyInjection;

namespace BSKTV.Service
{
    public static class ServiceCollectionExtensions
    {
        public static void AddKTVService(this IServiceCollection services)
        {
            services.AddKTVRepository();

            services.AddScoped<KugouService>();
            services.AddScoped<Net91Service>();
            services.AddScoped<SingerService>();
            services.AddScoped<DictionaryService>();
            services.AddScoped<SongService>();
            services.AddScoped<PlayListService>();
        }
    }
}
