﻿using BSKTV.Data;
using BSKTV.Model;
using BSKTV.Model.Input;
using BSKTV.Util;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BSKTV.Service
{
    public class SingerService : BaseService
    {
        private readonly Net91Service mv91NetService;
        private readonly KugouService kugouService;
        private readonly SingerRepository singerRepository;

        public SingerService(Net91Service mv91NetService, KugouService kugouService, SingerRepository singerRepository)
        {
            this.mv91NetService = mv91NetService;
            this.kugouService = kugouService;
            this.singerRepository = singerRepository;
        }

        public async Task<PagedModel<SingerModel>> GetPagedList(GetSingerPagedListInput input)
        {
            input.Check();

            var where = ExpressionUtil.Default<SingerModel>();

            string type = input.Type;
            string sexType = input.SexType;
            string keyword = input.Keyword;
            string keywordUpper = "";

            if (!string.IsNullOrEmpty(keyword))
            {
                keywordUpper = keyword.ToUpper();
            }

            where = where.MergeAnd(type != "0", m => m.Type == type);
            where = where.MergeAnd(sexType != "0", m => m.SexType == sexType);
            where = where.MergeAnd(!string.IsNullOrEmpty(keyword), m => m.SingerId == keyword || m.SingerNameSzm == keywordUpper || m.SingerName.Contains(keyword));

            return await singerRepository.GetPagedList(input, where, m => m.Id);
        }

        public async Task<SingerModel> UpdateSingerName(long id, string singerName)
        {
            SingerModel model = new SingerModel
            {
                Id = id,
                SingerName = singerName,
                SingerNameCode = WordUtil.GetPinyinForName(singerName),
                SingerNameSzm = WordUtil.GetFirstPinyin(singerName)
            };

            bool flag = await singerRepository.UpdateByIdAsync(model, m => new { m.SingerName, m.SingerNameCode, m.SingerNameSzm });

            return flag ? model : null;
        }

        public async Task<bool> AddList(List<SingerModel> list)
        {
            if (StringUtil.IsEmpty(list))
            {
                return false;
            }

            List<SingerModel> newList = new List<SingerModel>();
            foreach (var item in list)
            {
                var model = await GetInsertModel(item);

                if (model == null)
                {
                    continue;
                }

                newList.Add(model);
            }

            if (newList.Count > 0)
            {
                return singerRepository.InsertList(newList);
            }
            return true;
        }

        public async void Insert(SingerModel model)
        {
            var singer = await singerRepository.Get(m => m.SingerId == model.SingerId);
            if (singer == null)
            {
                var newModel = StringUtil.Copy(model);

                singerRepository.Insert(newModel);
            }
        }

        public async void Save(SingerModel model)
        {
            var singer = await singerRepository.Get(m => m.SingerId == model.SingerId);
            if (singer == null)
            {
                singer = StringUtil.Copy(model);

                singerRepository.Insert(singer);
            }
            else
            {
                singer.SingerId = model.SingerId;

                if (singer.SingerName != model.SingerName)
                {
                    singer.SingerName = model.SingerName;
                    singer.SingerNameCode = WordUtil.GetPinyin(singer.SingerName);
                    singer.SingerNameSzm = WordUtil.GetFirstPinyin(singer.SingerName);
                }

                await singerRepository.UpdateAsync(singer, m => m.Id == model.Id);
            }
        }

        private async Task<SingerModel> GetInsertModel(SingerModel model)
        {
            var singer = await singerRepository.Get(m => m.SingerId == model.SingerId);
            if (singer == null)
            {
                return StringUtil.Copy(model);
            }
            return null;
        }
    }
}
