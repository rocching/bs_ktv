﻿using BSKTV.Data;
using BSKTV.Model;
using BSKTV.Model.Input;
using BSKTV.Util;
using Roc.Data;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BSKTV.Service
{
    public class SongService
    {
        private readonly SongRepository songRepository;

        public SongService(SongRepository songRepository)
        {
            this.songRepository = songRepository;
        }

        public async Task<PagedModel<SongModel>> GetPagedList(GetSongPagedListInput input)
        {
            input.Check();

            var where = ExpressionUtil.Default<SongModel>();

            int type = input.Type;
            string source = input.Source;
            string keyword = input.Keyword;
            string keywordUpper = "";

            if (!string.IsNullOrEmpty(keyword))
            {
                keywordUpper = keyword.ToUpper();
            }

            where = where.MergeAnd(type > 0, m => m.Type == type);
            where = where.MergeAnd(!string.IsNullOrEmpty(source), m => m.Source == source);
            where = where.MergeAnd(!string.IsNullOrEmpty(keyword), m => m.SongId == keyword || m.SongNameSzm == keywordUpper || m.SingerName.Contains(keyword) || m.SongName.Contains(keyword));

            var pagedModel = await songRepository.GetPagedList(input, where, m => m.Id);
            if (pagedModel != null && pagedModel.HasData())
            {
                foreach (var item in pagedModel.List)
                {
                    string pic = item.Pic;

                    if (!string.IsNullOrEmpty(pic))
                    {
                        item.Pic = FileUtil.GetFilePath(pic);
                    }
                }
            }
            return pagedModel;
        }

        public async Task<List<SongModel>> GetList(string source, List<string> songIdList)
        {
            if (StringUtil.IsEmpty(songIdList))
            {
                return null;
            }

            var where = ExpressionUtil.Default<SongModel>();
            where = where.MergeAnd(m => m.Source == source);
            where = where.MergeAnd(m => SqlFunction.In(m.SongId, songIdList));

            return await songRepository.GetList(where);
        }

        public async Task<List<SongModel>> GetHandleList(int type, int count = 2)
        {
            if (count < 1)
            {
                count = 1;
            }

            string fieldName = type == Constant.SONG_HANDLE_TYPE_PIC ? "Pic" : "Url";

            string condition = $"ifnull({fieldName},'')='' order by id limit {count}";

            return await songRepository.GetList(condition);
        }

        public async Task<SongModel> Update(SongModel model)
        {
            model.SongNameCode = WordUtil.GetPinyinForName(model.SongName);
            model.SongNameSzm = WordUtil.GetFirstPinyin(model.SongName);

            bool flag = await songRepository.UpdateByIdAsync(model, m => new { m.SongName, m.SongNameCode, m.SongNameSzm, m.SingerName });

            return flag ? model : null;
        }

        public async Task<SongModel> UpdateSingerName(long id, string songName)
        {
            SongModel model = new SongModel
            {
                Id = id,
                SongName = songName,
                SongNameCode = WordUtil.GetPinyinForName(songName),
                SongNameSzm = WordUtil.GetFirstPinyin(songName)
            };

            bool flag = await songRepository.UpdateAsync(model, m => m.Id == id, m => new { m.SongName, m.SongNameCode, m.SongNameSzm });

            return flag ? model : null;
        }

        public async Task<bool> UpdateSongUrl(long id, string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return false;
            }

            SongModel model = new SongModel
            {
                Id = id,
                Url = url,
                MvUrl = url
            };

            bool flag = await songRepository.UpdateByIdAsync(model, m => new { m.Url, m.MvUrl });

            return flag;
        }

        public async Task<bool> UpdateSongPic(long id, string pic)
        {
            if (string.IsNullOrEmpty(pic))
            {
                return false;
            }

            SongModel model = new SongModel
            {
                Id = id,
                Pic = pic
            };

            return await songRepository.UpdateByIdAsync(model, m => new { m.Pic });
        }

        public async Task<bool> UpdateSongExtData(int id, string extData)
        {
            if (string.IsNullOrEmpty(extData))
            {
                return false;
            }

            SongModel model = new SongModel
            {
                Id = id,
                ExtData = extData
            };

            return await songRepository.UpdateByIdAsync(model, m => new { m.ExtData });
        }

        public async Task<bool> UpdateSongExtPic(long id, int sencod)
        {
            if (sencod <= 0)
            {
                return false;
            }

            SongExtModel extModel = new SongExtModel() { ScreenshotPosition = sencod };

            SongModel model = new SongModel
            {
                Id = id,
                Pic = "",
                ExtData = StringUtil.ToJson(extModel)
            };

            return await songRepository.UpdateByIdAsync(model, m => new { m.Pic, m.ExtData });
        }

        public async Task<bool> AddList(List<SongModel> list)
        {
            if (StringUtil.IsEmpty(list))
            {
                return false;
            }

            List<SongModel> newList = new List<SongModel>();
            foreach (var item in list)
            {
                var model = await GetInsertModel(item);

                if (model == null)
                {
                    continue;
                }

                model.Type = Constant.SONG_TYPE_DB;
                newList.Add(model);
            }

            if (newList.Count > 0)
            {
                return songRepository.InsertList(newList);
            }

            return true;
        }

        private async Task<SongModel> GetInsertModel(SongModel model)
        {
            var singer = await songRepository.Get(m => m.SongId == model.SongId);
            if (singer == null)
            {
                return StringUtil.Copy(model);
            }
            return null;
        }
    }
}
