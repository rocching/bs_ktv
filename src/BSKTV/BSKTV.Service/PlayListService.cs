﻿using BSKTV.Data;
using BSKTV.Model;
using BSKTV.Model.Input;
using BSKTV.Util;
using Roc.Data;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BSKTV.Service
{
    public class PlayListService
    {
        private readonly PlayListRepository playListRepository;

        public PlayListService(PlayListRepository playListRepository)
        {
            this.playListRepository = playListRepository;
        }

        public async Task<PagedModel<SongPlayListModel>> GetPagedList(GetSongPlayListPagedListInput input)
        {
            input.Check();

            var pagedModel = await playListRepository.GetPagedList(input);
            if (pagedModel != null && pagedModel.HasData())
            {
                foreach (var item in pagedModel.List)
                {
                    string pic = item.Pic;

                    if (!string.IsNullOrEmpty(pic))
                    {
                        item.Pic = FileUtil.GetFilePath(pic);
                    }
                }
            }

            return pagedModel;
        }

        #region 查询单个对象
        public async Task<PlayListModel> GetById(long playId)
        {
            string sqlText = "select * from T_PlayList where Id = ?id";

            Dictionary<string, object> map = new Dictionary<string, object>
            {
                { "id", playId }
            };

            return await playListRepository.GetAsync<PlayListModel>(sqlText, map);
        }

        public async Task<PlayListModel> GetBySongId(int songId, string clientId)
        {
            string sqlText = "select * from T_PlayList where SongId = ?songId and ClientId = ?clientId limit 1";

            Dictionary<string, object> map = new Dictionary<string, object>
            {
                { "songId", songId },
                { "clientId" , clientId }
            };

            return await playListRepository.GetAsync<PlayListModel>(sqlText, map);
        }

        public async Task<PlayListModel> GetNextBySortId(int sortId)
        {
            string sqlText = "select * from T_PlayList where SortId > ?sortId order by SortId limit 1";

            Dictionary<string, object> map = new Dictionary<string, object>
            {
                { "sortId", sortId }
            };

            return await playListRepository.GetAsync<PlayListModel>(sqlText, map);
        }

        public async Task<PlayListModel> GetPreBySortId(int sortId)
        {
            string sqlText = "select * from T_PlayList where SortId < ?sortId order by SortId desc limit 1";

            Dictionary<string, object> map = new Dictionary<string, object>
            {
                { "sortId", sortId }
            };

            return await playListRepository.GetAsync<PlayListModel>(sqlText, map);
        }

        #endregion

        public async Task<int> GetMaxSortId()
        {
            string sqlText = "select max(SortId) id from T_PlayList";

            int maxId = await playListRepository.GetAsync<int>(sqlText);

            if (maxId < Constant.DEFAULT_PLAY_LIST_SORTID)
            {
                maxId = Constant.DEFAULT_PLAY_LIST_SORTID;
            }
            return maxId;
        }

        public async Task<int> GetMinSortId()
        {
            string sqlText = "select min(SortId) id from T_PlayList";

            return await playListRepository.GetAsync<int>(sqlText);
        }

        public async Task<List<PlayListModel>> GetList(string clientId, List<long> idList)
        {
            var sql = playListRepository.GetSqlLam();
            sql.Where(m => m.ClientId == clientId).In(m => m.SongId, idList);

            return await playListRepository.GetListAsync(sql);
        }

        public async Task<bool> AddList(AddPlayListInput input)
        {
            input.Check();

            var maxId = await GetMaxSortId();

            var list = await GetList(input.ClientId, input.SongIdList);

            List<PlayListModel> addList = new List<PlayListModel>();
            foreach (int songId in input.SongIdList)
            {
                bool flag = HasAddSong(list, songId);

                if (flag)
                {
                    continue;
                }

                PlayListModel model = new PlayListModel
                {
                    SongId = songId,
                    ClientId = input.ClientId,
                    UserName = input.UserName
                };

                model.SortId = ++maxId;

                addList.Add(model);
            }

            if (addList.Count > 0)
            {
                return await playListRepository.InsertListAsync(addList);
            }

            return true;
        }

        public async Task<bool> Delete(long playId)
        {
            return await playListRepository.DeleteByIdAsync(playId);
        }

        public async Task<bool> BatchDelete(List<long> idList)
        {
            var where = ExpressionUtil.Default<PlayListModel>(m => SqlFunction.In(m.Id, idList));

            return await playListRepository.DeleteAsync(where);
        }

        public async Task Clear()
        {
            await playListRepository.TruncateAsync();
        }

        /// <summary>
        /// 向下移动
        /// </summary>
        /// <param name="playId"></param>
        /// <param name="sortId"></param>
        /// <returns></returns>
        public async Task<bool> MoveUpSong(long playId, int sortId)
        {
            var current = await GetById(playId);

            if (current == null)
            {
                return false;
            }

            var pre = await GetPreBySortId(sortId);

            if (pre == null)
            {
                return false;
            }

            current.SortId = pre.SortId;
            pre.SortId = sortId;

            await UpdateSongSortId(current);
            await UpdateSongSortId(pre);

            return true;
        }

        /// <summary>
        /// 向上移动
        /// </summary>
        /// <param name="playId"></param>
        /// <param name="sortId"></param>
        /// <returns></returns>
        public async Task<bool> MoveDownSong(long playId, int sortId)
        {
            var current = await GetById(playId);

            if (current == null)
            {
                return false;
            }

            var next = await GetNextBySortId(sortId);

            if (next == null)
            {
                return false;
            }

            current.SortId = next.SortId;
            next.SortId = sortId;

            await UpdateSongSortId(current);
            await UpdateSongSortId(next);

            return true;
        }

        public async Task<bool> MoveTop(long playId, int sortId)
        {
            var current = await GetById(playId);

            if (current == null)
            {
                return false;
            }

            int minSortId = await GetMinSortId();

            if (sortId <= minSortId)
            {
                return false;
            }

            current.SortId = --minSortId;

            await UpdateSongSortId(current);

            return true;
        }

        public async Task<bool> MoveBottom(long playId, int sortId)
        {
            var current = await GetById(playId);

            if (current == null)
            {
                return false;
            }

            int maxSortId = await GetMaxSortId();

            if (sortId >= maxSortId)
            {
                return false;
            }

            current.SortId = ++maxSortId;

            await UpdateSongSortId(current);

            return true;
        }

        private bool HasAddSong(List<PlayListModel> list, int songId)
        {
            if (list == null || list.Count < 1)
            {
                return false;
            }

            return list.Any(m => m.SongId == songId);
        }

        private async Task UpdateSongSortId(PlayListModel model)
        {
            await playListRepository.UpdateByIdAsync(model, m => new { m.SortId });
        }
    }
}
