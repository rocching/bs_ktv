﻿using System;
using System.Diagnostics;
using System.IO;

namespace BSKTV.Util
{
    public class FileUtil
    {
        public static bool ExistFile(string path)
        {
            return File.Exists(path);
        }

        public static void DeleteFile(string path)
        {
            try
            {
                File.Delete(path);
            }
            catch(Exception e)
            {
                LogUtil.Instance.Error(e);
            }
        }

        public static void Copy(string sourceFileName, string destFileName, bool overwrite = true)
        {
            File.Copy(sourceFileName, destFileName, overwrite);
        }

        public static string CopyToApp(string sourceFileName, string destDir, bool overwrite = true)
        {
            string fileName = Path.Combine(destDir, Path.GetFileName(sourceFileName));
            string destFileName = GetFilePath(fileName);
            string dir = Path.GetDirectoryName(destFileName);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            Copy(sourceFileName, destFileName, overwrite);
            return destFileName;
        }

        public static void CreateDirectory(string filePath)
        {
            string dir = Path.GetDirectoryName(filePath);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
        }

        public static string GetFilePath(string path)
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path);
        }

        public static string GetFileDir(string filePath)
        {
            return Path.GetDirectoryName(filePath);
        }

        public static string GetFilePath(string path, string fileName)
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path, fileName);
        }

        public static string GetParentFilePath(string path)
        {
            return Path.Combine(Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.FullName, path);
        }

        public static string GetParentPath()
        {
            return Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.FullName;
        }

        public static string GetFileName(string path)
        {
            string name = Path.GetFileName(path);
            return System.Net.WebUtility.UrlDecode(name);
        }

        public static string GetFileVersion(string path)
        {
            try
            {
                FileInfo file = new FileInfo(path);
                if (file != null && file.Exists)
                {
                    FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(file.FullName);
                    return versionInfo.FileVersion;
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return string.Empty;
            }
        }

        public static string ReadAllText(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return "";
            }
            try
            {
                return File.ReadAllText(path);
            }
            catch
            {
                return "";
            }
        }

        public static string[] ReadAllLines(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return null;
            }
            try
            {
                return File.ReadAllLines(path);
            }
            catch
            {
                return null;
            }
        }

        public static void WriteAllText(string path, string content)
        {
            if (string.IsNullOrEmpty(path))
            {
                return;
            }

            string dir = Path.GetDirectoryName(path);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            try
            {
                File.WriteAllText(path, content, System.Text.Encoding.UTF8);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
