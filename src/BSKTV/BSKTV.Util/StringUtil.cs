﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace BSKTV.Util
{
    public static class StringUtil
    {
        static StringUtil()
        {

        }

        /// <summary>
        /// 复制列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static ObservableCollection<T> CopyList<T>(List<T> list)
        {
            ObservableCollection<T> targetList = new ObservableCollection<T>();

            if (IsEmpty(list))
            {
                return new ObservableCollection<T>();
            }

            foreach (var item in list)
            {
                targetList.Add(item);
            }

            return targetList;
        }

        public static ObservableCollection<TTarget> CopyList<TSource, TTarget>(List<TSource> list) where TSource : class where TTarget : class
        {
            ObservableCollection<TTarget> targetList = new ObservableCollection<TTarget>();

            if (IsEmpty(list))
            {
                return new ObservableCollection<TTarget>();
            }

            foreach (var item in list)
            {
                targetList.Add(Copy<TSource, TTarget>(item));
            }

            return targetList;
        }

        public static List<T> CopyList<T>(IList<T> list)
        {
            if (IsEmpty(list))
            {
                return new List<T>();
            }

            string json = ToJson(list);

            return Parse<List<T>>(json);
        }

        public static List<TTarget> CopyList<TSource, TTarget>(IList<TSource> list)
        {
            if (IsEmpty(list))
            {
                return new List<TTarget>();
            }

            string json = ToJson(list);

            return Parse<List<TTarget>>(json);
        }

        /// <summary>
        /// 复制实体
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        /// <param name="model">实体</param>
        /// <returns></returns>
        public static T Copy<T>(T model) where T : class
        {
            if (model == null) return null;

            string json = ToJson(model);

            return Parse<T>(json);
        }

        public static TTarget Copy<TSource, TTarget>(TSource model) where TSource : class where TTarget : class
        {
            if (model == null) return null;

            string json = ToJson(model);

            return Parse<TTarget>(json);
        }

        public static bool IsEmpty<T>(List<T> list)
        {
            return list == null || list.Count < 1;
        }

        public static bool IsNotEmpty<T>(List<T> list)
        {
            return list != null && list.Count > 0;
        }

        public static string MD5Encrypt32(string text, bool md5_16 = false)
        {
            if (string.IsNullOrEmpty(text))
            {
                return string.Empty;
            }

            //密文
            string pwd = string.Empty;
            //实例化一个md5对像
            MD5 md5 = MD5.Create();
            // 加密后是一个字节类型的数组，这里要注意编码UTF8/Unicode等的选择　
            byte[] s = md5.ComputeHash(Encoding.UTF8.GetBytes(text));
            // 通过使用循环，将字节类型的数组转换为字符串，此字符串是常规字符格式化所得
            for (int i = 0; i < s.Length; i++)
            {
                // 将得到的字符串使用十六进制类型格式。
                //格式后的字符是小写的字母，如果使用大写（X）则格式后的字符是大写字符
                //X2表示16进制
                pwd = pwd + s[i].ToString("X2");
            }

            if (md5_16)
            {
                pwd = pwd.Substring(4, 16);
            }
            return pwd;
        }

        public static T Parse<T>(string json, bool camelCase = false)
        {
            if (string.IsNullOrEmpty(json))
            {
                return default;
            }
            try
            {
                JsonSerializerSettings jss = new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                };

                if (camelCase)
                {
                    jss.ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver();
                }
                return JsonConvert.DeserializeObject<T>(json, jss);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return default;
            }
        }
        //Formatting = Formatting.Indented,
        public static string ToJson(object obj, bool camelCase = false)
        {
            JsonSerializerSettings jss = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };

            if (camelCase)
            {
                jss.ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver();
            }
            return JsonConvert.SerializeObject(obj, Formatting.None, jss);
        }

        public static double ToDouble(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return 0;
            }
            if (double.TryParse(s, out double d))
                return d;
            return 0;
        }

        public static double ToFixed(string s, int digits = 2)
        {
            double d = ToDouble(s);
            return ToFixed(d, digits);
        }

        public static double ToFixed(double f, int digits = 2)
        {
            return Math.Round(f, digits, MidpointRounding.AwayFromZero);
        }

        public static string ToPercent(double d, int digits = 2)
        {
            double dd = ToFixed(d, digits + 2);
            return $"{dd * 100}%";
        }

        public static string ToEyeString(double d, bool eyeOpend, bool isTotal = false)
        {
            if (eyeOpend)
            {
                if (d == 0 && !isTotal)
                {
                    return "--";
                }
                return d.ToString();
            }

            if (d == 0)
            {
                return "--";
            }

            if (d < 0)
            {
                d *= -1;
            }

            return ToEyeString(d.ToString(), eyeOpend);
        }

        public static string ToEyeString(string d, bool eyeOpend)
        {
            if (eyeOpend)
            {
                return d;
            }
            return Regex.Replace(d, @"[\d]", "*").Replace("%", "");
        }

        public static long GetTimeStamp()
        {
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return (long)ts.TotalMilliseconds;
        }

        public static string GetVersionString(Type type)
        {
            return GetVersion(type).ToString();
        }

        public static Version GetVersion(Type type)
        {
            return type.Assembly.GetName().Version;
        }

        public static Version GetVersion(string versionString)
        {
            if (string.IsNullOrEmpty(versionString))
            {
                versionString = "";
            }
            versionString = versionString.Replace("v", "").Replace("V", "");

            if (Version.TryParse(versionString, out Version version))
            {
                return version;
            }
            return new Version(1, 0, 0, 0);
        }

        public static bool IsEmpty<T>(ICollection<T> list)
        {
            return list == null || list.Count < 1;
        }

        public static bool IsNotEmpty<T>(ICollection<T> list)
        {
            return list != null && list.Count > 0;
        }

        public static int Random(int min = 0, int max = 100)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        public static string UrlDecode(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return string.Empty;
            }
            return System.Net.WebUtility.UrlDecode(url);
        }
    }
}
