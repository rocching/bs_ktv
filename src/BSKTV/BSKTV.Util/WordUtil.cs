﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolGood.Words.Pinyin;

namespace BSKTV.Util
{
    public class WordUtil
    {
        /// <summary>
        /// 获得拼音
        /// </summary>
        /// <param name="word">汉字</param>
        /// <param name="split">分隔符</param>
        /// <param name="sd">声调</param>
        /// <returns></returns>
        public static string GetPinyin(string word, string split = "", bool sd = false)
        {
            return WordsHelper.GetPinyin(word, split, sd);
        }

        /// <summary>
        /// 获得首字母
        /// </summary>
        /// <param name="word">汉字</param>
        /// <returns></returns>
        public static string GetFirstPinyin(string word)
        {
            return WordsHelper.GetFirstPinyin(word);
        }

        /// <summary>
        /// 获取全部拼音
        /// </summary>
        /// <param name="word">汉字</param>
        /// <param name="sd">声调</param>
        /// <returns></returns>
        public static List<string> GetAllPinyin(char word, bool sd = false)
        {
            return WordsHelper.GetAllPinyin(word, sd);
        }

        /// <summary>
        /// 获取姓名
        /// </summary>
        /// <param name="word">汉字</param>
        /// <param name="split">分割符号</param>
        /// <param name="sd">声调</param>
        /// <returns></returns>
        public static string GetPinyinForName(string word, string split = "", bool sd = false)
        {
            return WordsHelper.GetPinyinForName(word, split, sd);
        }

        /// <summary>
        /// 清理缓存
        /// </summary>
        public static void ClearCache()
        {
            WordsHelper.ClearCache();
        }
    }
}
