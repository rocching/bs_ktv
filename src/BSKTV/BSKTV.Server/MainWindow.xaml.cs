﻿using BSKTV.Server.ViewModel;
using System.ComponentModel;

namespace BSKTV.Server
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            Init();
        }

        private void Init()
        {
            MainViewModel vm = App.GetService<MainViewModel>();
            vm.MainWindow = this;

            vm.Load();

            DataContext = vm;

            InitializeComponent();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (DataContext is MainViewModel vm)
            {
                if (vm.ShutDownApp)
                {
                    base.OnClosing(e);
                }
                else
                {
                    Hide();

                    e.Cancel = true;
                }
            }
        }

        //private void OnLeftMainContentShiftIn(object sender, RoutedEventArgs e)
        //{
        //    GridSplitter.IsEnabled = true;

        //    double targetValue = ColumnDefinitionLeft.Width.Value;

        //    DoubleAnimation animation = AnimationHelper.CreateAnimation(targetValue, milliseconds: 100);
        //    animation.FillBehavior = FillBehavior.Stop;
        //    animation.Completed += OnAnimationCompleted;
        //    LeftMainContent.RenderTransform.BeginAnimation(TranslateTransform.XProperty, animation);

        //    void OnAnimationCompleted(object _, EventArgs args)
        //    {
        //        animation.Completed -= OnAnimationCompleted;
        //        LeftMainContent.RenderTransform.SetCurrentValue(TranslateTransform.XProperty, targetValue);

        //        Grid.SetColumn(MainContent, 1);
        //        Grid.SetColumnSpan(MainContent, 1);

        //        ColumnDefinitionLeft.MinWidth = 240;
        //        ColumnDefinitionLeft.Width = _columnDefinitionWidth;
        //    }
        //}
    }
}
