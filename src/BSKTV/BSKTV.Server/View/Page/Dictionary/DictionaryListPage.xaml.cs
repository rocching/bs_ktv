﻿using BSKTV.Server.ViewModel.Dictionary;
namespace BSKTV.Server.View.Page.Dictionary
{
    /// <summary>
    /// DictionaryListPage.xaml 的交互逻辑
    /// </summary>
    public partial class DictionaryListPage
    {
        public DictionaryListPage(DictionaryListViewModel viewModel)
        {
            DataContext = viewModel;

            InitializeComponent();
        }
    }
}
