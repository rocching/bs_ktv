﻿using BSKTV.Server.ViewModel.Song;

namespace BSKTV.Server.View.Page.Song
{
    /// <summary>
    /// Net91SongListPage.xaml 的交互逻辑
    /// </summary>
    public partial class Net91SongListPage
    {
        public Net91SongListPage(Net91SongListViewModel viewModel)
        {
            ViewModel = viewModel;
            DataContext = viewModel;

            InitializeComponent();
        }

        public Net91SongListViewModel ViewModel { get; private set; }
    }
}
