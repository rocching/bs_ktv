﻿using BSKTV.Server.ViewModel.Song;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BSKTV.Server.View.Page.Song
{
    /// <summary>
    /// PlayListPage.xaml 的交互逻辑
    /// </summary>
    public partial class PlayListPage
    {
        public PlayListPage(PlayListViewModel viewModel)
        {
            InitializeComponent();

            //viewModel.Initing = player =>
            //{
            //    ((ISupportInitialize)player).BeginInit();

            //    playerHost.Child.Controls.Add(player);

            //    ((ISupportInitialize)player).EndInit();
            //};

            DataContext = viewModel;
        }
    }
}
