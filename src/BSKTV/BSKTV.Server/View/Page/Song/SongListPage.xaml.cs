﻿using BSKTV.Server.ViewModel.Song;

namespace BSKTV.Server.View.Page.Song
{
    /// <summary>
    /// SongListPage.xaml 的交互逻辑
    /// </summary>
    public partial class SongListPage
    {
        public SongListPage(SongListViewModel viewModel)
        {
            InitializeComponent();

            this.ViewModel = viewModel;
            this.DataContext = viewModel;
        }

        public SongListViewModel ViewModel { get; set; }
    }
}
