﻿using BSKTV.Server.ViewModel;

namespace BSKTV.Server.View.Page
{
    /// <summary>
    /// HomePage.xaml 的交互逻辑
    /// </summary>
    public partial class HomePage
    {
        public HomeViewModel ViewModel { get; set; }

        public HomePage()
        {
            DataContext = ViewModel;

            InitializeComponent();
        }
    }
}
