﻿using BSKTV.Server.ViewModel.Server;
using BSKTV.Util;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Config;
using System.Collections.ObjectModel;
using System.Linq;

namespace BSKTV.Server.Server
{
    public static class KtvServerMgr
    {
        private static KtvAppServer appServer = null;

        public static void Start(string ip, int port)
        {
            var state = GetServerState();

            if (state != ServerState.NotStarted)
            {
                LogUtil.Instance.WriteLog("Ktv server has been start.");
                return;
            }

            var se = new ServerConfig
            {
                TextEncoding = "UTF-8",
                Ip = ip,
                Port = port,
                ClearIdleSession = false,
                MaxRequestLength = 40960,
                SendBufferSize = 4096,
                MaxConnectionNumber = 1000
            };

            appServer = new KtvAppServer();

            if (!appServer.Setup(se))
            {
                LogUtil.Instance.Error("Ktv server setup fail.");
                return;
            }

            bool flag = appServer.Start();

            if (!flag)
            {
                LogUtil.Instance.Error("Ktv server start fail.");
            }
        }

        public static void Stop()
        {
            appServer?.Stop();
        }

        public static ObservableCollection<ClientViewModel> GetAllClientList()
        {
            ObservableCollection<ClientViewModel> list = new ObservableCollection<ClientViewModel>();
            var sessions = appServer?.GetAllSessions();

            if (sessions != null && sessions.Count() > 0)
            {
                foreach (var session in sessions)
                {
                    ClientViewModel model = new ClientViewModel();
                    model.SessionID = session.SessionID;
                    model.Connected = session.Connected;
                    model.StartTime = session.StartTime;
                    model.LastActiveTime = session.LastActiveTime;
                    model.IpAddress = session.RemoteEndPoint.Address.ToString();
                    model.Port = session.RemoteEndPoint.Port;
                    list.Add(model);
                }
            }

            return list;
        }

        public static ServerState GetServerState()
        {
            if (appServer == null)
            {
                return ServerState.NotStarted;
            }


            return appServer.State;
        }

        public static string GetServerStateString()
        {
            var state = GetServerState();
            string s = "";
            switch (state)
            {
                case ServerState.NotStarted:
                    s = "未启动";
                    break;
                case ServerState.Starting:
                    s = "启动中...";
                    break;
                case ServerState.Initializing:
                    s = "初始化中...";
                    break;
                case ServerState.Running:
                    s = "运行中";
                    break;
                case ServerState.Stopping:
                    s = "停止中...";
                    break;
            }
            return s;
        }
    }
}
