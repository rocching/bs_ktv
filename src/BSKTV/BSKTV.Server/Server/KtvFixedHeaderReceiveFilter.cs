﻿using BSKTV.Model;
using SuperSocket.Facility.Protocol;
using SuperSocket.SocketBase.Protocol;
using System;
using System.Text;

namespace BSKTV.Server.Server
{
    public class KtvFixedHeaderReceiveFilter : FixedHeaderReceiveFilter<StringRequestInfo>
    {
        public KtvFixedHeaderReceiveFilter() : base(8)
        {

        }

        protected override int GetBodyLengthFromHeader(byte[] header, int offset, int length)
        {
            var strLen = Encoding.UTF8.GetString(header, offset + 4, 4);
            return int.Parse(strLen.TrimStart('0'));
        }

        protected override StringRequestInfo ResolveRequestInfo(ArraySegment<byte> header, byte[] bodyBuffer, int offset, int length)
        {
            string str = Encoding.UTF8.GetString(bodyBuffer, offset, length);

            var parser = new BasicRequestInfoParser(Constant.KtvServer.CMD_SPLIT_CHAR, Constant.KtvServer.PARAMETER_SPLIT_CHAR);

            return parser.ParseRequestInfo(str);
        }
    }
}
