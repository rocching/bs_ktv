﻿using BSKTV.Model;
using BSKTV.Util;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Protocol;
using System.Text;

namespace BSKTV.Server.Server
{
    public class KtvAppServer : AppServer<KtvAppSession>
    {
        public KtvAppServer() : base(new TerminatorReceiveFilterFactory(Constant.KtvServer.END_CHAR, Encoding.UTF8, new BasicRequestInfoParser(Constant.KtvServer.CMD_SPLIT_CHAR, Constant.KtvServer.PARAMETER_SPLIT_CHAR)))
        {

        }

        //public KtvAppServer() : base(new KtvFixedHeaderReceiveFilterFactory())
        //{
        //return string.Format("ECHO{0}{1}", sourceLine.Length.ToString().PadLeft(4), sourceLine);
        //}

        public override bool Start()
        {
            LogUtil.Instance.Info("Ktv server start.");
            return base.Start();
        }

        public override void Stop()
        {
            LogUtil.Instance.Info("Ktv server stop.");
            base.Stop();
        }
    }
}
