﻿using BSKTV.Util;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Protocol;

namespace BSKTV.Server.Server
{
    public class KtvAppSession : AppSession<KtvAppSession>
    {
        protected override void OnSessionStarted()
        {
            LogUtil.Instance.Info($"有新客户端连接,{this.SessionID}");
        }

        protected override void HandleUnknownRequest(StringRequestInfo requestInfo)
        {
            Send("Unknown request: " + requestInfo.Key);
        }
    }
}
