﻿using BSKTV.Model;
using BSKTV.Util;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Nodes;

namespace BSKTV.Server.Server
{
    public class CmdBuilder
    {
        private bool flag = true;
        private string cmd;
        private string body;
        private readonly List<string> parameterList;

        public CmdBuilder()
        {
            cmd = string.Empty;
            body = string.Empty;
            parameterList = new List<string>();
        }

        public CmdBuilder Flag(bool flag)
        {
            this.flag = flag;

            return this;
        }

        public CmdBuilder Cmd(string cmd)
        {
            if (!string.IsNullOrEmpty(cmd))
            {
                this.cmd = cmd;
            }
            return this;
        }

        public CmdBuilder Body(string body)
        {
            if (!string.IsNullOrEmpty(body))
            {
                this.body = body;
            }
            return this;
        }

        public CmdBuilder Body(object data)
        {
            if (data == null)
            {
                return this;
            }

            return Body(StringUtil.ToJson(data, true));
        }

        public CmdBuilder AddParameter(string p)
        {
            if (string.IsNullOrEmpty(p))
            {
                return this;
            }

            parameterList.Add(p);

            return this;
        }

        public string Build()
        {
            if (string.IsNullOrEmpty(body))
            {
                if (parameterList.Count > 0)
                {
                    body = string.Join(Constant.KtvServer.PARAMETER_SPLIT_CHAR, parameterList);
                }
            }

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("cmd", cmd);
            dic.Add("body", body);

            string bodyString = StringUtil.ToJson(dic, true);

            bodyString = Convert.ToBase64String(Encoding.UTF8.GetBytes(bodyString));

            string str = Constant.KtvServer.END_CHAR + bodyString + Constant.KtvServer.END_CHAR;
            return str;
        }

        public string BuildOld()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(cmd);

            if (flag)
            {
                sb.Append(Constant.KtvServer.CMD_SPLIT_CHAR);
            }

            if (string.IsNullOrEmpty(body))
            {
                if (parameterList.Count > 0)
                {
                    sb.Append(string.Join(Constant.KtvServer.PARAMETER_SPLIT_CHAR, parameterList));
                }
            }
            else
            {
                sb.Append(body);
            }

            if (flag)
            {
                sb.Append(Constant.KtvServer.END_CHAR);
            }

            Clear();

            string bodyString = sb.ToString();

            return bodyString.Length.ToString().PadLeft(4, ' ') + bodyString;
        }

        private void Clear()
        {
            this.cmd = string.Empty;
            this.body = string.Empty;
            this.parameterList.Clear();
        }

        private static CmdBuilder builder = null;
        private static readonly object lockObj = new object();

        public static CmdBuilder New()
        {
            lock (lockObj)
            {
                if (builder == null)
                {
                    builder = App.GetService<CmdBuilder>();
                }
            }

            return builder;
        }
    }
}
