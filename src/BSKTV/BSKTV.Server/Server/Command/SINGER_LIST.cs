﻿using BSKTV.Model.Input;
using BSKTV.Service;
using BSKTV.Util;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;

namespace BSKTV.Server.Server.Command
{
    public class SINGER_LIST : CommandBase<KtvAppSession, StringRequestInfo>
    {
        private readonly SingerService singerService;

        public SINGER_LIST()
        {
            this.singerService = App.GetService<SingerService>();
        }

        public override async void ExecuteCommand(KtvAppSession session, StringRequestInfo requestInfo)
        {
            GetSingerPagedListInput input = StringUtil.Parse<GetSingerPagedListInput>(requestInfo.Body) ?? new GetSingerPagedListInput();

            input.Check();

            var pagedModel = await singerService.GetPagedList(input);

            string msg = CmdBuilder.New().Cmd(nameof(SINGER_LIST)).Body(pagedModel).Build();

            session.Send(msg);
        }
    }
}
