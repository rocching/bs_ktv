﻿using BSKTV.Model.Input;
using BSKTV.Service;
using BSKTV.Util;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;

namespace BSKTV.Server.Server.Command
{
    public class PLAY_LIST : CommandBase<KtvAppSession, StringRequestInfo>
    {
        private readonly PlayListService playListService;
        
        public PLAY_LIST()
        {
            playListService = App.GetService<PlayListService>();
        }

        public override async void ExecuteCommand(KtvAppSession session, StringRequestInfo requestInfo)
        {
            GetSongPlayListPagedListInput input = StringUtil.Parse<GetSongPlayListPagedListInput>(requestInfo.Body) ?? new GetSongPlayListPagedListInput();

            input.Check();

            var pagedModel = await playListService.GetPagedList(input);

            string msg = CmdBuilder.New().Cmd(nameof(PLAY_LIST)).Body(pagedModel).Build();

            session.Send(msg);
        }
    }
}
