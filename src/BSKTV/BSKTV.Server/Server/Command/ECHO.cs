﻿using BSKTV.Model;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;

namespace BSKTV.Server.Server.Command
{
    public class ECHO : CommandBase<KtvAppSession, StringRequestInfo>
    {
        public override void ExecuteCommand(KtvAppSession session, StringRequestInfo requestInfo)
        {
            string msg = CmdBuilder.New().Cmd(nameof(ECHO)).Body(HeartInfo.Success()).Build();

            session.Send(msg);
        }
    }
}
