﻿using BSKTV.Server.Core;
using BSKTV.Server.Schedule;
using BSKTV.Server.Server;
using BSKTV.Server.View.Dialog;
using BSKTV.Server.View.Page;
using BSKTV.Server.View.Page.Dictionary;
using BSKTV.Server.View.Page.Server;
using BSKTV.Server.View.Page.Singer;
using BSKTV.Server.View.Page.Song;
using BSKTV.Server.ViewModel;
using BSKTV.Server.ViewModel.Dialog;
using BSKTV.Server.ViewModel.Dictionary;
using BSKTV.Server.ViewModel.Server;
using BSKTV.Server.ViewModel.Singer;
using BSKTV.Server.ViewModel.Song;
using BSKTV.Service;
using BSKTV.Util;
using CommunityToolkit.Mvvm.DependencyInjection;
using FreeScheduler;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.ComponentModel;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace BSKTV.Server
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        private static IServiceCollection services;
        private static IServiceProvider serviceProvider;
        private Mutex mutex;

        protected override void OnStartup(StartupEventArgs e)
        {
            string name = Assembly.GetExecutingAssembly().GetName().Name;
            mutex = new Mutex(true, name, out bool flag);
            if (flag)
            {
                ConfigureServices();
                RegisterErrorHandler();
            }
            else
            {
                var p = ProcessUtil.GetCurrentProcess();
                Win32Util.ShowWindowAsync(p.MainWindowHandle, 1);
                Win32Util.SetForegroundWindow(p.MainWindowHandle);

                Environment.Exit(0);
            }
        }

        private static void ConfigureServices()
        {
            services = new ServiceCollection();

            services.AddSingleton<PageService>();
            services.AddSingleton<FFmpegService>();
            services.AddSingleton<CmdBuilder>();

            services.AddScoped<HomePage>();
            services.AddScoped<SingerListPage>();
            services.AddScoped<KgSingerListPage>();
            services.AddScoped<SongListPage>();
            services.AddScoped<PlayListPage>();
            services.AddScoped<Net91SongListPage>();
            services.AddScoped<DictionaryListPage>();
            services.AddScoped<ServerPage>();

            services.AddScoped<OneTextDialog>();

            services.AddSingleton<MainViewModel>();

            services.AddScoped<HomeViewModel>();
            services.AddScoped<SingerListViewModel>();
            services.AddScoped<KgSingerListViewModel>();
            services.AddScoped<SongListViewModel>();
            services.AddScoped<PlayListViewModel>();
            services.AddScoped<Net91SongListViewModel>();
            services.AddScoped<DictionaryListViewModel>();
            services.AddScoped<DictionaryViewModel>();
            services.AddScoped<ServerViewModel>();
            services.AddScoped<ClientViewModel>();

            services.AddScoped<OneTextDialogViewModel>();

            services.AddKTVService();

            services.AddScoped<SongUrlTaskHandler>();
            services.AddScoped<SongPicTaskHandler>();
            services.AddScoped<MyScheduler>();

            serviceProvider = services.BuildServiceProvider();

            services.AddSingleton(serviceProvider);

            Ioc.Default.ConfigureServices(serviceProvider);
        }

        public static T GetService<T>() where T : class
        {
            return serviceProvider.GetService(typeof(T)) as T;
        }

        public static IServiceProvider GetProvider()
        {
            return serviceProvider;
        }

        private void RegisterErrorHandler()
        {
            this.DispatcherUnhandledException += App_DispatcherUnhandledException;//UI线程异常
            TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException;//Task线程异常
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (e.ExceptionObject != null)
            {
                if (e.ExceptionObject is Exception ex)
                {
                    LogUtil.Instance.Error(ex);
                }
            }
        }

        private void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            LogUtil.Instance.Error(e.Exception);
            e.SetObserved();
        }

        private void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            LogUtil.Instance.Error(e.Exception);
            e.Handled = true;
        }
    }
}
