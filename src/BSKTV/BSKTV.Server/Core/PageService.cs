﻿using BSKTV.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSKTV.Server
{
    public class PageService
    {
        private static readonly string NameSpaceString = typeof(PageService).Assembly.GetName().Name;

        private readonly IServiceProvider serviceProvider;
        public PageService(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public object CreatePage(string className)
        {
            try
            {
                var type = Type.GetType($"{NameSpaceString}.View.Page.{className}");
                //return type == null ? null : Activator.CreateInstance(type);

                return serviceProvider.GetService(type);
            }
            catch (Exception e)
            {
                LogUtil.Instance.Error(e);
                return null;
            }
        }
    }
}
