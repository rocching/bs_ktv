﻿using BSKTV.Server.ViewModel.Song;
using CommunityToolkit.Mvvm.Messaging.Messages;

namespace BSKTV.Server.Core.Message
{
    public class RefreshPlayListMessage : ValueChangedMessage<PlayedSongViewModel>
    {
        public RefreshPlayListMessage(PlayedSongViewModel value) : base(value)
        {

        }
    }
}
