﻿using BSKTV.Model;
using BSKTV.Server.Util;
using BSKTV.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSKTV.Server.Core
{
    public class FFmpegService
    {
        public FFmpegService() { }

        /// <summary>
        /// 截屏
        /// </summary>
        /// <param name="model">歌曲实体</param>
        /// <param name="second">时间</param>
        public bool Screenshot(SongModel model, int second)
        {
            if (string.IsNullOrEmpty(model.SongId))
            {
                return false;
            }

            if (string.IsNullOrEmpty(model.Url))
            {
                return false;
            }

            string imagePath = AppUtil.GetScreenshotPath(model.SongId);

            if (FileUtil.ExistFile(imagePath))
            {
                FileUtil.DeleteFile(imagePath);
            }

            //AddMusicToTrackList(model);

            string cmd = $"ffmpeg -ss {second} -i \"{model.Url}\" -frames:v 1 -y {imagePath}";

            Task.Factory.StartNew(() =>
            {
                HandleCmd(cmd);
            });

            return true;
        }

        private void HandleCmd(string cmd)
        {
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo.FileName = "cmd.exe";
            p.StartInfo.UseShellExecute = false;    //是否使用操作系统shell启动
            p.StartInfo.RedirectStandardInput = true;//接受来自调用程序的输入信息
            p.StartInfo.RedirectStandardOutput = true;//由调用程序获取输出信息
            p.StartInfo.RedirectStandardError = true;//重定向标准错误输出
            p.StartInfo.CreateNoWindow = true;//不显示程序窗口
            p.StartInfo.WorkingDirectory = $"{Environment.CurrentDirectory}\\ffmpeg";

            p.Start();

            //string cmd = $"ffmpeg -ss {second} -i \"{url}\" -frames:v 1 -y {imagePath}";

            LogUtil.Instance.WriteLog($"执行命令: {cmd}");

            p.StandardInput.WriteLine(cmd + "&exit");

            p.StandardInput.AutoFlush = true;

            //string output = p.StandardOutput.ReadToEnd();
            string output = p.StandardError.ReadToEnd();

            p.WaitForExit();//等待程序执行完退出进程
            p.Close();

            LogUtil.Instance.WriteLog("输出:" + output);
        }
    }
}
