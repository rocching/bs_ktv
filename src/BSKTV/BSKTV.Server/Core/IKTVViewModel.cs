﻿namespace BSKTV.Server.Core
{
    public interface IKTVViewModel
    {
        bool NeedRefresh { get; set; }

        void Init();

        void Refresh();
    }
}
