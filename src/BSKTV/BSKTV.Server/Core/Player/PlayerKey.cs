﻿namespace BSKTV.Server.Core.Player
{
    public class PlayerKey
    {
        /// <summary>
        /// 左键点击
        /// </summary>
        public const int WM_LBUTTONDOWN = 0x201; //定义了鼠标的左键点击消息 
        /// <summary>
        /// 左键弹起
        /// </summary>
        public const int WM_LBUTTONUP = 0x202;  //定义了鼠标左键弹起消息
        /// <summary>
        /// 左键双击
        /// </summary>
        public const int WM_LBUTTONDBLCLK = 0x203; //定义了鼠标的左键双击消息 
        /// <summary>
        /// 右键按下
        /// </summary>
        public const int WM_RBUTTONDOWN = 0x0204;//定义了鼠标的右键按下消息
    }
}
