﻿using AxAPlayer3Lib;
using HandyControl.Tools;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace BSKTV.Server.Core.Player
{
    public static class PlayerMgr
    {
        private static AxPlayer player;
        private static Form container;
        private static string currentUrl;
        private static string currentTitle;

        private static int formWidth = 1000;
        private static int formHeight = 500;

        static PlayerMgr()
        {
            InitForm();

            InitPlayer();
        }

        private static void InitPlayer()
        {
            player = new AxPlayer();
            player.Enabled = true;
            player.Location = new Point(0, 0);
            player.Name = "ktv_player";
            player.Width = container.Width;
            player.Height = container.Height;
            player.TabIndex = 0;
            player.Dock = DockStyle.Fill;

            player.OnMessage += Player_OnMessage;
            player.OnBuffer += Player_OnBuffer;
            player.OnOpenSucceeded += Player_OnOpenSucceeded;
            player.OnStateChanged += Player_OnStateChanged;

            ((ISupportInitialize)player).BeginInit();

            container.Controls.Add(player);

            ((ISupportInitialize)player).EndInit();
        }

        private static void InitForm()
        {
            container = new Form
            {
                Width = formWidth,
                Height = formHeight,
                ShowInTaskbar = false,
                //Icon = Icon.FromHandle(App.Current.MainWindow.GetHandle()),
                WindowState = FormWindowState.Normal,
                FormBorderStyle = FormBorderStyle.Sizable,
                StartPosition = FormStartPosition.CenterScreen
            };

            container.FormClosing += Container_FormClosing;

            container.Hide();
        }

        private static void Container_FormClosing(object sender, FormClosingEventArgs e)
        {
            container.Hide();
            e.Cancel = true;
        }

        #region public method

        public static Action<PlayStatus> StatusChanged { get; set; }

        public static PlayStatus GetStatus()
        {
            if (player == null)
            {
                return PlayStatus.PS_READY;
            }

            int state = player.GetState();

            return (PlayStatus)state;
        }

        public static void Play(string url, string title)
        {
            if (player == null)
            {
                return;
            }

            if (string.IsNullOrEmpty(url))
            {
                return;
            }

            currentUrl = url;
            currentTitle = title;

            container.Text = $"正在播放 - {title}";

            player.Open(url);
        }

        public static void Play()
        {
            if (player == null)
            {
                return;
            }

            player.Play();
        }

        public static void PlayOrPause()
        {
            var status = GetStatus();

            switch (status)
            {
                case PlayStatus.PS_READY:
                case PlayStatus.PS_CLOSING:
                    Play(currentUrl, currentTitle);
                    break;
                case PlayStatus.PS_PAUSED:
                case PlayStatus.PS_PAUSING:
                    Play();
                    break;
                case PlayStatus.PS_OPENING:
                case PlayStatus.PS_PLAY:
                case PlayStatus.PS_PLAYING:
                    Pause();
                    break;
            }
        }

        public static void Stop()
        {
            if (player == null)
            {
                return;
            }

            player.Close();
        }

        public static void Pause()
        {
            if (player == null)
            {
                return;
            }

            player.Pause();
        }

        public static void SetPosition(double position)
        {
            if (player == null)
            {
                return;
            }
            player.SetPosition((int)position * 1000);
        }

        public static void SetVolume(int volume)
        {
            if (player == null)
            {
                return;
            }

            player.SetVolume(volume);
        }

        public static int GetVolume()
        {
            if (player == null)
            {
                return 50;
            }

            return player.GetVolume();
        }

        public static int GetDuration()
        {
            if (player == null)
            {
                return 0;
            }
            return player.GetDuration();
        }

        public static int GetPosition()
        {
            if (player == null)
            {
                return 0;
            }

            return player.GetPosition();
        }

        public static string GetConfig(int cmd)
        {
            if (player == null)
            {
                return null;
            }

            return player.GetConfig(cmd);
        }

        public static void Show()
        {
            if (container.Visible)
            {
                container.Visible = false;
            }
            else
            {
                container.Visible = true;
                //container.Show();
                container.WindowState = FormWindowState.Normal;
            }
        }
        #endregion

        private static void Player_OnStateChanged(object sender, _IPlayerEvents_OnStateChangedEvent e)
        {
            PlayStatus state = (PlayStatus)e.nNewState;
            switch (state)
            {
                case PlayStatus.PS_READY:
                    break;
                case PlayStatus.PS_OPENING:
                    break;
                case PlayStatus.PS_PLAY:
                case PlayStatus.PS_PLAYING:
                    break;
                case PlayStatus.PS_PAUSING:
                case PlayStatus.PS_PAUSED:
                    break;
                case PlayStatus.PS_CLOSING:
                    break;
            }

            StatusChanged?.Invoke(state);
        }

        private static void Player_OnMessage(object sender, _IPlayerEvents_OnMessageEvent e)
        {
            switch (e.nMessage)
            {
                case PlayerKey.WM_LBUTTONDBLCLK:
                    ChangeScreen();
                    break;
                case PlayerKey.WM_LBUTTONUP:
                    PlayOrPause();
                    break;
            }
        }

        private static void Player_OnBuffer(object sender, _IPlayerEvents_OnBufferEvent e)
        {

        }

        private static void Player_OnOpenSucceeded(object sender, EventArgs e)
        {
            //int count = GetMusicTrackCount();
            //if (count < 2)
            //{
            //    LoadMp3Bz(_currentMusic);
            //}

            //string trackList = player.GetConfig(402);
            //LogUtil.Instance.WriteLog($"当前播放[{CurrentMusic.Title}],音轨列表:[{trackList}]");
        }

        private static void ChangeScreen()
        {
            var state = container.WindowState;

            if (state == FormWindowState.Maximized)
            {
                container.FormBorderStyle = FormBorderStyle.Sizable;
                container.WindowState = FormWindowState.Normal;
            }
            else
            {
                var screen = Screen.FromControl(container);

                container.FormBorderStyle = FormBorderStyle.None;
                container.WindowState = FormWindowState.Maximized;
                container.Width = screen.Bounds.Width;
                container.Height = screen.Bounds.Height;

                player.Width = container.Width;
                player.Height = container.Height;
            }
        }
    }
}
