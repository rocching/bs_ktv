﻿namespace BSKTV.Server.Core.Player
{
    public enum PlayStatus
    {
        /// <summary>
        /// 正在加载
        /// </summary>
        PS_READY = 0,
        /// <summary>
        /// 正在打开
        /// </summary>
        PS_OPENING = 1,
        /// <summary>
        /// 正在暂停
        /// </summary>
        PS_PAUSING = 2,
        /// <summary>
        /// 暂停
        /// </summary>
        PS_PAUSED = 3,
        /// <summary>
        /// 正在播放
        /// </summary>
        PS_PLAYING = 4,
        /// <summary>
        /// 播放
        /// </summary>
        PS_PLAY = 5,
        /// <summary>
        /// 正在关闭
        /// </summary>
        PS_CLOSING = 6,
    }
}
