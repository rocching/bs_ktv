﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace BSKTV.Server.Core
{
    public class MyDataGridAttach
    {
        public static DependencyProperty ShowRowNumberProperty = DependencyProperty.RegisterAttached("ShowRowNumber", typeof(bool), typeof(MyDataGridAttach), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.Inherits, OnShowRowNumberChanged));

        public static bool GetShowRowNumber(DependencyObject target) => (bool)target.GetValue(ShowRowNumberProperty);

        public static void SetShowRowNumber(DependencyObject target, bool value) => target.SetValue(ShowRowNumberProperty, value);

        private static void OnShowRowNumberChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            if (!(target is DataGrid dataGrid)) return;

            UpdateNumber(dataGrid);
        }

        private static void DataGrid_LoadingRow(object sender, DataGridRowEventArgs e) => e.Row.Header = (e.Row.GetIndex() + 1).ToString();

        public static DependencyProperty PageIndexProperty = DependencyProperty.RegisterAttached("PageIndex", typeof(int), typeof(MyDataGridAttach), new FrameworkPropertyMetadata(1, FrameworkPropertyMetadataOptions.Inherits, OnPageIndexChanged));

        public static int GetPageIndex(DependencyObject target) => (int)target.GetValue(PageIndexProperty);

        public static void SetPageIndex(DependencyObject target, int value) => target.SetValue(PageIndexProperty, value);

        private static void OnPageIndexChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            if (!(target is DataGrid dataGrid)) return;

            UpdateNumber(dataGrid);
        }

        public static DependencyProperty PageSizeProperty = DependencyProperty.RegisterAttached("PageSize", typeof(int), typeof(MyDataGridAttach), new FrameworkPropertyMetadata(10, FrameworkPropertyMetadataOptions.Inherits, null));

        public static int GetPageSize(DependencyObject target) => (int)target.GetValue(PageSizeProperty);

        public static void SetPageSize(DependencyObject target, int value) => target.SetValue(PageSizeProperty, value);

        private static void UpdateNumber(DataGrid dataGrid)
        {
            bool show = GetShowRowNumber(dataGrid);

            if (show)
            {
                dataGrid.LoadingRow += DataGrid_LoadingRow;
                dataGrid.ItemContainerGenerator.ItemsChanged += ItemContainerGeneratorItemsChanged;
            }
            else
            {
                dataGrid.LoadingRow -= DataGrid_LoadingRow;
                dataGrid.ItemContainerGenerator.ItemsChanged -= ItemContainerGeneratorItemsChanged;
            }

            void ItemContainerGeneratorItemsChanged(object sender, ItemsChangedEventArgs ee)
            {
                var generator = dataGrid.ItemContainerGenerator;
                var itemsCount = dataGrid.Items.Count;

                int pageIndex = GetPageIndex(dataGrid);
                int pageSize = GetPageSize(dataGrid);

                int begin = (pageIndex - 1) * pageSize;

                if (show)
                {
                    for (var i = 0; i < itemsCount; i++)
                    {
                        var row = (DataGridRow)generator.ContainerFromIndex(i);
                        if (row != null)
                        {
                            row.Header = (begin + i + 1).ToString();
                        }
                    }
                }
                else
                {
                    for (var i = 0; i < itemsCount; i++)
                    {
                        var row = (DataGridRow)generator.ContainerFromIndex(i);
                        if (row != null)
                        {
                            row.Header = null;
                        }
                    }
                }
            }
        }
    }
}
