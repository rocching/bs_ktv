﻿using BSKTV.Model;
using BSKTV.Server.Core;
using BSKTV.Server.Util;
using BSKTV.Service;
using BSKTV.Util;
using FreeScheduler;
using System.Threading.Tasks;

namespace BSKTV.Server.Schedule
{
    public class SongPicTaskHandler : DefaultTaskHandler
    {
        private readonly SongService songService;
        private readonly FFmpegService ffmpegService;
        public SongPicTaskHandler(SongService songService, FFmpegService ffmpegService)
        {
            this.songService = songService;
            this.ffmpegService = ffmpegService;
        }

        public override async void OnExecuting(Scheduler scheduler, TaskInfo task)
        {
            base.OnExecuting(scheduler, task);

            string code = task.Code;

            if (code == Constant.Task.MUSCI_PIC_TASK)
            {
                await ExecutingConvertPic(scheduler);
            }
            else if (code == Constant.Task.MUSIC_PIC_CHECK_TASK)
            {
                await UpdateMusicPic(scheduler, task);
            }
        }

        private async Task UpdateMusicPic(Scheduler scheduler, TaskInfo task)
        {
            SongModel model = StringUtil.Parse<SongModel>(task.Body);

            if (model == null)
            {
                return;
            }

            string filePath = AppUtil.GetScreenshotPath(model.SongId);

            if (FileUtil.ExistFile(filePath))
            {
                string relativePath = AppUtil.GetMusicImageRelativePath(model.SongId);

                await songService.UpdateSongPic(model.Id, relativePath);

                scheduler.RemoveTask(task.Id);
            }
        }

        private async Task ExecutingConvertPic(Scheduler scheduler)
        {
            var list = await songService.GetHandleList(Constant.SONG_HANDLE_TYPE_PIC);

            if (list != null && list.Count > 0)
            {
                foreach (var model in list)
                {
                    var extInfo = model.GetExtInfo();

                    bool flag = ffmpegService.Screenshot(model, extInfo.ScreenshotPosition);

                    if (flag)
                    {
                        string data = StringUtil.ToJson(model);

                        scheduler.AddTask(Constant.Task.MUSIC_PIC_CHECK_TASK, "检查图片任务", data, 10, 2);//执行10次,每2秒一次
                    }
                }
            }
        }
    }
}
