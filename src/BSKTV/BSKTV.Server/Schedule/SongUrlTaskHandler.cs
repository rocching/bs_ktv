﻿using BSKTV.Model;
using BSKTV.Service;
using BSKTV.Util;
using FreeScheduler;
using System.Threading.Tasks;

namespace BSKTV.Server.Schedule
{
    public class SongUrlTaskHandler : DefaultTaskHandler
    {
        private readonly SongService songService;
        private readonly Net91Service net91Service;
        public SongUrlTaskHandler(SongService songService, Net91Service net91Service)
        {
            this.songService = songService;
            this.net91Service = net91Service;
        }

        public override async void OnExecuting(Scheduler scheduler, TaskInfo task)
        {
            base.OnExecuting(scheduler, task);

            var list = await songService.GetHandleList(Constant.SONG_HANDLE_TYPE_URL);

            if (list != null && list.Count > 0)
            {
                foreach (var model in list)
                {
                    string url = await GetSongUrlAsync(model.SongId);

                    if (string.IsNullOrEmpty(url))
                    {
                        continue;
                    }

                    await songService.UpdateSongUrl(model.Id, url);
                }
            }
        }

        private async Task<string> GetSongUrlAsync(string songId)
        {
            string url = await net91Service.GetMvUrl(Constant.MV91_VIP_USER_ACCOUNT, songId);

            if (string.IsNullOrEmpty(url))
            {
                LogUtil.Instance.Error($"无法更新歌曲播放地址-{songId}");
                return null;
            }

            return url;
        }
    }
}
