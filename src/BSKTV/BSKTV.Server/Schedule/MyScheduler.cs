﻿using BSKTV.Model;
using FreeScheduler;

namespace BSKTV.Server.Schedule
{
    public class MyScheduler
    {
        public MyScheduler()
        {

        }

        public void Run()
        {
            var s1 = new Scheduler(App.GetService<SongUrlTaskHandler>());

            s1.AddTask(Constant.Task.MUSIC_URL_TASK, "获得歌曲播放地址任务", "", -1, 5);

            var s2 = new Scheduler(App.GetService<SongPicTaskHandler>());

            s2.AddTask(Constant.Task.MUSCI_PIC_TASK, "获得歌曲图片任务", "", -1, 10);
        }
    }
}
