﻿using BSKTV.Util;
using FreeScheduler;
using System;
using System.Collections.Generic;

namespace BSKTV.Server.Schedule
{
    public class DefaultTaskHandler : ITaskHandler
    {
        public TaskInfo Load(string id)
        {
            return null;
        }

        public IEnumerable<TaskInfo> LoadAll()
        {
            return new TaskInfo[0];
        }

        public void OnAdd(TaskInfo task)
        {

        }

        public void OnRemove(TaskInfo task)
        {

        }

        public virtual void OnExecuted(Scheduler scheduler, TaskInfo task, TaskLog result)
        {

        }

        public virtual void OnExecuting(Scheduler scheduler, TaskInfo task)
        {
            string msg = $"[{DateTime.Now.ToString("HH:mm:ss")}] {task.Topic} 被执行";
            LogUtil.Instance.WriteLog(msg);
        }
    }
}
