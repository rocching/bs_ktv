﻿using BSKTV.Util;
using System;
using System.IO;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace BSKTV.Server.Converter
{
    public class ImgConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            string path = value.ToString();

            if (string.IsNullOrEmpty(path))
            {
                return null;
            }

            if (!FileUtil.ExistFile(path))
            {
                return null;
            }

            return InitImage(path);

            //try
            //{
            //    using (BinaryReader loader = new BinaryReader(new FileStream(path, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite)))
            //    {
            //        FileInfo fd = new FileInfo(path);
            //        int Length = (int)fd.Length;
            //        byte[] buf = loader.ReadBytes((int)fd.Length);

            //        loader.Dispose();
            //        loader.Close(); //开始加载图像

            //        BitmapImage bim = new BitmapImage();
            //        bim.BeginInit();
            //        bim.StreamSource = new MemoryStream(buf);
            //        bim.EndInit();
            //        GC.Collect();
            //        return bim;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    LogUtil.Instance.Error(ex);
            //    return null;
            //}
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return parameter;
        }

        /// <summary>
        /// 解决不同进程读取同一张图片的问题
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private BitmapImage InitImage(string filePath)
        {
            BitmapImage bitmapImage;
            using (BinaryReader reader = new BinaryReader(File.Open(filePath, FileMode.Open)))
            {
                FileInfo fi = new FileInfo(filePath);
                byte[] bytes = reader.ReadBytes((int)fi.Length);
                reader.Close();

                //image = new Image();
                bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = new MemoryStream(bytes);
                bitmapImage.EndInit();
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                //image.Source = bitmapImage;
                reader.Dispose();
            }
            return bitmapImage;
        }
    }
}
