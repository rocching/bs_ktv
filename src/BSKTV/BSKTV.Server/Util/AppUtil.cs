﻿using BSKTV.Model;
using BSKTV.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BSKTV.Server.Util
{
    public class AppUtil
    {
        /// <summary>
        /// 获得歌曲图片相对路径
        /// </summary>
        /// <param name="songId">歌曲ID</param>
        /// <returns></returns>
        public static string GetMusicImageRelativePath(string songId)
        {
            return $"{Constant.MUSIC_LIST_NAME}\\Images\\{songId}.png";
        }

        public static string GetScreenshotPath(string songId)
        {
            string path = GetMusicImageRelativePath(songId);

            string filePath = FileUtil.GetFilePath(path);

            string dir = FileUtil.GetFileDir(filePath);

            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            return filePath;
        }

        /// <summary>
        /// 获得本地IP地址列表
        /// </summary>
        /// <returns></returns>
        public static List<string> GetIpList()
        {
            var ipList = Dns.GetHostAddresses(Dns.GetHostName());

            List<string> list = new List<string>();
            foreach (var ip in ipList)
            {
                if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    list.Add(ip.ToString());
                }
            }

            return list;
        }
    }
}
