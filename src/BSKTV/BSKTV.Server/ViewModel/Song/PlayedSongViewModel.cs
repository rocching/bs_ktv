﻿using BSKTV.Server.Core.Message;
using BSKTV.Service;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using HandyControl.Controls;

namespace BSKTV.Server.ViewModel.Song
{
    public class PlayedSongViewModel : SongViewModel
    {
        private long playId;
        private string clientId;
        private string userName;
        private int sortId;

        public long PlayId { get => playId; set => SetProperty(ref playId, value); }

        /// <summary>
        /// 客户端ID
        /// </summary>
        public string ClientId { get => clientId; set => SetProperty(ref clientId, value); }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get => userName; set => SetProperty(ref userName, value); }

        /// <summary>
        /// 排序ID
        /// </summary>
        public int SortId { get => sortId; set => SetProperty(ref sortId, value); }

        public RelayCommand<string> MoveSongCmd => new RelayCommand<string>(MoveSong);

        public RelayCommand DeleteSongCmd => new RelayCommand(DeleteSong);

        private async void MoveSong(string type)
        {
            var service = App.GetService<PlayListService>();

            bool flag = false;
            switch (type)
            {
                case "1":
                    flag = await service.MoveUpSong(PlayId, SortId);
                    break;
                case "2":
                    flag = await service.MoveDownSong(PlayId, SortId);
                    break;
                case "3":
                    flag = await service.MoveTop(PlayId, SortId);
                    break;
                case "4":
                    flag = await service.MoveBottom(PlayId, SortId);
                    break;
            }

            SendMsg(flag);
        }

        private void DeleteSong()
        {
            Growl.Clear();

            Growl.Ask($"确实要删除[{SongName}]吗?", (confirm) =>
             {
                 if (confirm)
                 {
                     DeleteSongInnerAsync();
                 }

                 return true;
             });

            async void DeleteSongInnerAsync()
            {
                var service = App.GetService<PlayListService>();

                bool flag = await service.Delete(PlayId);

                SendMsg(flag);
            }
        }

        private void SendMsg(bool flag)
        {
            if (flag)
            {
                WeakReferenceMessenger.Default.Send(new RefreshPlayListMessage(this));
            }
        }
    }
}
