﻿using BSKTV.Model;
using BSKTV.Model.Input;
using BSKTV.Server.Core;
using BSKTV.Server.Core.Message;
using BSKTV.Server.Core.Player;
using BSKTV.Service;
using BSKTV.Util;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using HandyControl.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BSKTV.Server.ViewModel.Song
{
    public class PlayListViewModel : DefaultPageViewModel<PlayedSongViewModel>
    {
        private readonly DictionaryService dictionaryService;
        private readonly PlayListService playListService;

        public PlayListViewModel(PlayListService playListService, DictionaryService dictionaryService)
        {
            this.playListService = playListService;
            this.dictionaryService = dictionaryService;

            base.NeedRefresh = true;
        }

        private int _type;

        public int Type { get => _type; set => SetProperty(ref _type, value); }

        private ObservableCollection<DictionaryModel> _typeList;
        public ObservableCollection<DictionaryModel> TypeList { get => _typeList; private set => SetProperty(ref _typeList, value); }

        public RelayCommand BatchDeleteCmd => new RelayCommand(BatchDelete);

        public RelayCommand ClearCmd => new RelayCommand(Clear);

        public override async void Init()
        {
            await LoadTypeData();

            RegisterMessage();

            InitTimer();

            InitPlayer();

            base.Init();
        }

        public override async void LoadData(int pageIndex)
        {
            GetSongPlayListPagedListInput input = new GetSongPlayListPagedListInput
            {
                Type = Type,
                Keyword = Keyword,
                PageIndex = pageIndex,
                PageSize = PageSize
            };

            var pagedModel = await playListService.GetPagedList(input);

            TotalPage = pagedModel.GetTotalPage();
            DataList = StringUtil.CopyList<SongPlayListModel, PlayedSongViewModel>(pagedModel.List);
        }

        protected override List<long> GetSelectedIdList()
        {
            return GetSelectedList().Select(m => m.PlayId).ToList();
        }

        private async Task LoadTypeData()
        {
            var typeList = await dictionaryService.GetList(Constant.SONG_TYPE);

            TypeList = StringUtil.CopyList(typeList);
        }

        private void BatchDelete()
        {
            if (HasSelectedItem())
            {
                Growl.Clear();

                Growl.Ask("确实要删除选择的歌曲吗?", confirm =>
                {
                    if (confirm)
                    {
                        BatchDeleteInner();
                    }
                    return true;
                });

                async void BatchDeleteInner()
                {
                    var idList = GetSelectedIdList();

                    var flag = await playListService.BatchDelete(idList);

                    if (flag)
                    {
                        Refresh();
                    }
                }
            }
            else
            {
                Growl.Warning("还没有选择要删除的歌曲!");
            }
        }

        private void Clear()
        {
            if (HasData())
            {
                Growl.Clear();

                Growl.Ask("确实要清空播放列表吗?", confirm =>
                {
                    if (confirm)
                    {
                        ClearInner();
                    }
                    return true;
                });

                async void ClearInner()
                {
                    await playListService.Clear();

                    Refresh();
                }
            }
        }

        private void RegisterMessage()
        {
            WeakReferenceMessenger.Default.Register<RefreshPlayListMessage>(this, (r, m) =>
            {
                Growl.Success("更新成功...");

                Refresh();
            });
        }

        #region 播放器

        private void InitPlayer()
        {
            CurrentSencod = 0;
            TotalSencod = 100;

            PlayerMgr.StatusChanged = (status) =>
            {
                ChangeStatus(status);

                CurrentStatus = status;
            };

            MusicVolume = 50;

            keyboardHook = new KeyboardHookLib();
            keyboardHook.KeyPressEvent += KeyboardHook_KeyPressEvent;
            keyboardHook.Start();
        }

        private void KeyboardHook_KeyPressEvent(object sender, KeyPressEventArgs e)
        {
            int code = (int)e.KeyChar;
            //截获Ctrl+Up
            if (code == 43 && (int)Control.ModifierKeys == (int)Keys.Shift)
            {
                AddVolume();

                return;
            }
  
            //截获Ctrl+Up
            if (code == 95 && (int)Control.ModifierKeys == (int)Keys.Shift)
            {
                SubVolume();

                return;
            }
        }

        private void InitTimer()
        {
            _timer = new System.Timers.Timer
            {
                Interval = 500,
                Enabled = true
            };
            _timer.Elapsed += Timer_Elapsed;
            _timer.Start();
        }

        private readonly string timeSpanFormat = @"mm\:ss";
        private int currentIndex = -1;
        private PlayedSongViewModel currentMusic;
        private double totalSencod;
        private double currentSencod;
        private PlayStatus currentStatus;
        private bool isPlaying;
        private int _musicVolume;

        private KeyboardHookLib keyboardHook;

        private System.Timers.Timer _timer;

        private string _progressText;
        private TimeSpan _totalTimeSpan;
        private TimeSpan _currentTimeSpan;

        public PlayedSongViewModel CurrentMusic { get => currentMusic; set => SetProperty(ref currentMusic, value); }

        //public int CurrentIndex { get => currentIndex; set => currentIndex = value; }

        public double TotalSencod { get => totalSencod; set => SetProperty(ref totalSencod, value); }

        public double CurrentSencod { get => currentSencod; set => SetProperty(ref currentSencod, value); }

        public PlayStatus CurrentStatus { get => currentStatus; set => SetProperty(ref currentStatus, value); }

        public bool IsPlaying { get => isPlaying; set => SetProperty(ref isPlaying, value); }

        public TimeSpan TotalTime { get => _totalTimeSpan; set => SetProperty(ref _totalTimeSpan, value); }

        public TimeSpan CurrentTime { get => _currentTimeSpan; set => SetProperty(ref _currentTimeSpan, value); }

        public string ProgressText { get => _progressText; set => SetProperty(ref _progressText, value); }

        public int MusicVolume
        {
            get { return _musicVolume; }
            set
            {
                SetProperty(ref _musicVolume, value);

                PlayerMgr.SetVolume(value);
            }
        }

        public RelayCommand PlayCmd => new RelayCommand(Play);

        public RelayCommand PlayNewCmd => new RelayCommand(PlayNew);

        public RelayCommand PlayNextCmd => new RelayCommand(PlayNext);

        public RelayCommand PlayPreCmd => new RelayCommand(PlayPre);

        public RelayCommand WatchMvCmd => new RelayCommand(WatchMv);

        private void PlayNew()
        {
            var first = GetSelectedFirst();

            if (first == null)
            {
                Growl.Warning("还没有选择要播放的歌曲!");
            }
            else
            {
                currentIndex = GetItemIndex(first);

                Play(first);
            }
        }

        private void Play()
        {
            var status = PlayerMgr.GetStatus();

            switch (status)
            {
                case PlayStatus.PS_READY:
                case PlayStatus.PS_CLOSING:
                    RePlay();
                    break;
                case PlayStatus.PS_PAUSED:
                case PlayStatus.PS_PAUSING:
                    PlayerMgr.Play();
                    break;
                case PlayStatus.PS_OPENING:
                case PlayStatus.PS_PLAY:
                case PlayStatus.PS_PLAYING:
                    PlayerMgr.Pause();
                    break;
            }
        }

        private void Play(PlayedSongViewModel model)
        {
            string url = model.MvUrl;

            if (string.IsNullOrEmpty(url))
            {
                return;
            }

            CurrentMusic = model;
            PlayerMgr.Play(url, model.SongName);
        }

        private void PlayNext()
        {
            if (DataList.Count < 1)
            {
                return;
            }

            if (currentIndex < DataList.Count - 1)
            {
                currentIndex++;
            }
            else
            {
                currentIndex = 0;
            }

            Play(GetItemByIndex(currentIndex));
        }

        private void PlayPre()
        {
            if (DataList.Count < 1)
            {
                return;
            }

            if (currentIndex > 0)
            {
                currentIndex--;
            }
            else
            {
                currentIndex = DataList.Count - 1;
            }

            Play(GetItemByIndex(currentIndex));
        }

        private void WatchMv()
        {
            PlayerMgr.Show();
        }

        private void RePlay()
        {
            if (currentMusic != null && !string.IsNullOrEmpty(currentMusic.MvUrl))
            {
                Play(currentMusic);
                return;
            }

            PlayNext();
        }

        private void ChangeStatus(PlayStatus status)
        {
            switch (status)
            {
                case PlayStatus.PS_READY:
                    IsPlaying = false;
                    string str = PlayerMgr.GetConfig(7);
                    if (string.IsNullOrEmpty(str))
                    {
                        str = "1";
                    }

                    LogUtil.Instance.WriteLog($"the str is {str}.");

                    if (str == "1" || str == "0")
                    {
                        SetCurrentTime();

                        PlayNext();
                    }
                    else
                    {
                        Play(CurrentMusic);//重播
                    }
                    break;
                case PlayStatus.PS_PAUSED:
                case PlayStatus.PS_PAUSING:
                    IsPlaying = false;
                    break;
                case PlayStatus.PS_OPENING:
                    IsPlaying = true;
                    SetCurrentTime();
                    break;
                case PlayStatus.PS_PLAY:
                case PlayStatus.PS_PLAYING:
                    IsPlaying = true;
                    SetCurrentTime();
                    break;
                case PlayStatus.PS_CLOSING:
                    IsPlaying = false;
                    break;
            }
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (IsPlaying)
            {
                Do(() =>
                {
                    SetCurrentTime();
                });
            }
        }

        private void SetCurrentTime()
        {
            int position = PlayerMgr.GetPosition();

            CurrentSencod = position;
            CurrentTime = TimeSpan.FromMilliseconds(position);

            int duration = PlayerMgr.GetDuration();

            TotalSencod = duration;
            TotalTime = TimeSpan.FromMilliseconds(duration);

            SetProgressText();
        }

        private void SetProgressText()
        {
            ProgressText = $"{CurrentTime.ToString(timeSpanFormat)} / {TotalTime.ToString(timeSpanFormat)}";
        }

        private void AddVolume()
        {
            int volume = MusicVolume + Constant.DEFAULT_MUSIC_VOLUME_STEP;

            if (volume > Constant.DEFAULT_MUSIC_MAX_VOLUME)
            {
                volume = Constant.DEFAULT_MUSIC_MAX_VOLUME;
            }

            MusicVolume = volume;
        }

        private void SubVolume()
        {
            int volume = MusicVolume - Constant.DEFAULT_MUSIC_VOLUME_STEP;

            if (volume < Constant.DEFAULT_MUSIC_MIN_VOLUME)
            {
                volume = Constant.DEFAULT_MUSIC_MIN_VOLUME;
            }

            MusicVolume = volume;
        }
        #endregion
    }
}
