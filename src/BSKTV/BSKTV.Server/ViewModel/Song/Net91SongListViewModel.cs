﻿using BSKTV.Model;
using BSKTV.Model.Input;
using BSKTV.Service;
using BSKTV.Util;
using CommunityToolkit.Mvvm.Input;
using HandyControl.Controls;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

namespace BSKTV.Server.ViewModel.Song
{
    public class Net91SongListViewModel : DefaultPageViewModel<SongViewModel>
    {
        private readonly Net91Service net91Service;
        private readonly SongService songService;
        public Net91SongListViewModel(SongService songService, DictionaryService dictionaryService, Net91Service net91Service)
        {
            this.net91Service = net91Service;
            this.songService = songService;
        }

        public RelayCommand AddByPageCmd => new RelayCommand(AddByPage);

        public RelayCommand AddCmd => new RelayCommand(Add);

        public override void Init()
        {
            PageSize = Constant.MV91_SONG_PAGE_SIZE;

            PageIndex = 1;
        }

        public async override void LoadData(int pageIndex)
        {
            if (string.IsNullOrEmpty(Keyword))
            {
                TotalPage = 1;
                return;
            }

            GetSongPagedListInput input = new GetSongPagedListInput
            {
                Type = 0,
                Keyword = Keyword,
                PageIndex = pageIndex,
                PageSize = PageSize
            };

            var pagedModel = await net91Service.GetPagedList(input, null);

            TotalPage = pagedModel.GetTotalPage();
            DataList = StringUtil.CopyList<SongModel, SongViewModel>(pagedModel.List);

            if (!pagedModel.HasData())
            {
                Growl.Warning("没有找到任何数据.");
            }
        }

        protected override void GridSelectionChanged(SelectionChangedEventArgs args)
        {


            if (args.Source is DataGrid grid)
            {
                foreach (var item in grid.SelectedItems)
                {
                    if (item is SongViewModel model)
                    {
                        if (model.Type == Constant.SONG_TYPE_CLOUD)
                        {
                            AddItem(StringUtil.Copy(model));
                        }
                    }
                }
            }
        }

        private void Add()
        {
            if (HasSelectedItem())
            {
                Add(GetSelectedList());
            }
            else
            {
                Growl.Warning("还没有选择要添加的歌曲!");
            }
        }

        private void AddByPage()
        {
            List<SongViewModel> list = new List<SongViewModel>();

            foreach (var item in DataList)
            {
                if (item.Type == Constant.SONG_TYPE_CLOUD)
                {
                    list.Add(StringUtil.Copy(item));
                }
            }

            if (list.Count > 0)
            {
                Add(list);
            }
        }

        private async void Add(IList<SongViewModel> addList)
        {
            if (addList == null || addList.Count < 1)
            {
                Growl.Warning("歌曲列表为空!");
                return;
            }

            var list = StringUtil.CopyList<SongViewModel, SongModel>(addList);

            bool flag = await songService.AddList(list);

            if (flag)
            {
                UpdateSongStatus(list);

                Growl.Success("添加91MV歌曲成功!");
            }
            else
            {
                Growl.Error("添加91MV歌曲失败!");
            }
        }

        private void UpdateSongStatus(List<SongModel> list)
        {
            foreach (var item in list)
            {
                var model = DataList.FirstOrDefault(m => m.SongId == item.SongId && m.Source == item.Source);

                if (model != null)
                {
                    model.Type = Constant.SONG_TYPE_DB;
                }
            }
        }
    }
}
