﻿using BSKTV.Model;
using BSKTV.Model.Input;
using BSKTV.Server.View.Dialog;
using BSKTV.Server.ViewModel.Dialog;
using BSKTV.Service;
using BSKTV.Util;
using CommunityToolkit.Mvvm.Input;
using HandyControl.Controls;
using HandyControl.Tools.Extension;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace BSKTV.Server.ViewModel.Song
{
    public class SongListViewModel : DefaultPageViewModel<SongViewModel>
    {
        private readonly SongService songService;
        private readonly DictionaryService dictionaryService;
        private readonly PlayListService playListService;
        public SongListViewModel(SongService songService, DictionaryService dictionaryService, PlayListService playListService)
        {
            this.songService = songService;
            this.dictionaryService = dictionaryService;
            this.playListService = playListService;

            PageSize = 10;
        }

        public override async void Init()
        {
            await LoadTypeData();

            PageIndex = 1;
        }

        private SongViewModel editVm = null;

        private int _type;

        public int Type { get => _type; set => SetProperty(ref _type, value); }

        private ObservableCollection<DictionaryModel> _typeList;
        public ObservableCollection<DictionaryModel> TypeList { get => _typeList; private set => SetProperty(ref _typeList, value); }

        public RelayCommand<DataGridBeginningEditEventArgs> BeginningEditCmd => new RelayCommand<DataGridBeginningEditEventArgs>(BeginningEdit);

        public RelayCommand<DataGridCellEditEndingEventArgs> CellEditEndingCmd => new RelayCommand<DataGridCellEditEndingEventArgs>(CellEditEnding);

        public RelayCommand<DataGridRowEditEndingEventArgs> RowEditEndingCmd => new RelayCommand<DataGridRowEditEndingEventArgs>(RowEditEnding);

        public RelayCommand<SongViewModel> RepeatScreenshotCmd => new RelayCommand<SongViewModel>(RepeatScreenshot);

        public RelayCommand AddSongCmd => new RelayCommand(AddSong);

        public override async void LoadData(int pageIndex)
        {
            GetSongPagedListInput input = new GetSongPagedListInput
            {
                Type = Type,
                Keyword = Keyword,
                PageIndex = pageIndex,
                PageSize = PageSize
            };

            var pagedModel = await songService.GetPagedList(input);

            TotalPage = pagedModel.GetTotalPage();
            DataList = StringUtil.CopyList<SongModel, SongViewModel>(pagedModel.List);
        }

        private async Task LoadTypeData()
        {
            var typeList = await dictionaryService.GetList(Constant.SONG_TYPE);

            TypeList = StringUtil.CopyList(typeList);
        }

        private void AddSong()
        {
            if (HasSelectedItem())
            {
                AddSong(GetSelectedList());
            }
            else
            {
                Growl.Warning("还没有选择要添加的歌曲!");
            }
        }

        private void BeginningEdit(DataGridBeginningEditEventArgs e)
        {
            if (e.Row.DataContext is SongViewModel viewModel)
            {
                editVm = StringUtil.Copy(viewModel);
            }
        }

        private async void RowEditEnding(DataGridRowEditEndingEventArgs e)
        {
            if (e.EditAction == DataGridEditAction.Commit)
            {
                if (e.Row.DataContext is SongViewModel vm)
                {
                    if (!HasChange(vm))
                    {
                        return;
                    }

                    var model = StringUtil.Copy<SongViewModel, SongModel>(vm);

                    var newModel = await songService.Update(model);

                    if (newModel != null)
                    {
                        vm.SongName = newModel.SongName;
                        vm.SongNameCode = newModel.SongNameCode;
                        vm.SongNameSzm = newModel.SongNameSzm;
                        vm.SingerName = newModel.SingerName;

                        Growl.Success("歌曲更新成功!");
                    }
                    else
                    {
                        Growl.Error("歌曲更新失败!");
                    }
                }
            }
        }

        private bool HasChange(SongViewModel vm)
        {
            if (editVm != null)
            {
                if (editVm.SongName != vm.SongName)
                {
                    return true;
                }

                if (editVm.SingerName != vm.SingerName)
                {
                    return true;
                }

                return false;
            }

            return true;
        }

        private void CellEditEnding(DataGridCellEditEndingEventArgs e)
        {
            if (e.EditAction == DataGridEditAction.Commit)
            {
                if (e.Row.DataContext is SongViewModel vm)
                {

                }
            }
        }

        private async void RepeatScreenshot(SongViewModel model)
        {
            if (model == null)
            {
                Growl.Warning("没有选择歌曲!");
                return;
            }

            var extInfo = model.GetExtInfo();
            var vm = App.GetService<OneTextDialogViewModel>();

            vm.Result = extInfo.ScreenshotPosition;
            vm.LeftTitle = "设置截屏位置";
            vm.Title = model.SongName;

            var content = App.GetService<OneTextDialog>();
            content.DataContext = vm;

            var result = await HandyControl.Controls.Dialog.Show(content).GetResultAsync<int>();

            if (result > 0)
            {
                await songService.UpdateSongExtPic(model.Id, result);
            }
        }

        private async void AddSong(List<SongViewModel> list)
        {
            var idList = list.Select(m => m.Id).ToList();

            AddPlayListInput input = new AddPlayListInput()
            {
                ClientId = Constant.DEFAULT_SERVER_ID,
                UserName = Constant.DEFAULT_SERVER_NAME,
                SongIdList = idList
            };

            bool flag = await playListService.AddList(input);

            if (flag)
            {
                //UpdateSongStatus(list);

                Growl.Success("点歌成功!");
            }
            else
            {
                Growl.Error("点歌失败!");
            }
        }
    }
}
