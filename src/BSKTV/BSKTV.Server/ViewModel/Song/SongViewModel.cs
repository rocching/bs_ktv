﻿using BSKTV.Model;
using BSKTV.Service;
using BSKTV.Util;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using HandyControl.Controls;
using HandyControl.Tools;
using System;

namespace BSKTV.Server.ViewModel.Song
{
    public class SongViewModel : ObservableObject, IPrimaryKey
    {
        public SongViewModel() { }

        private long id;
        private string source;
        private string songId;
        private string songName;
        private int type;
        private string typeName;
        private string songNameCode;
        private string songNameSzm;
        private string singerName;
        private string localPath;
        private string pic;
        private string url;
        private string mvUrl;

        public long Id { get => id; set => SetProperty(ref id, value); }

        public string Source { get => source; set => SetProperty(ref source, value); }

        public string SongId { get => songId; set => SetProperty(ref songId, value); }

        public string SongName { get => songName; set => SetProperty(ref songName, value); }

        public int Type
        {
            get => type;
            set
            {
                SetProperty(ref type, value);

                TypeName = SongModel.GetTypeName(type);
            }
        }

        public string SongNameSzm { get => songNameSzm; set => SetProperty(ref songNameSzm, value); }

        public string SongNameCode { get => songNameCode; set => SetProperty(ref songNameCode, value); }

        public string SingerName { get => singerName; set => SetProperty(ref singerName, value); }

        public string LocalPath { get => localPath; set => SetProperty(ref localPath, value); }

        public string Url { get => url; set => SetProperty(ref url, value); }

        public string Pic
        {
            get
            {
                //if (string.IsNullOrEmpty(pic))
                //{
                //    pic = GetDefaultPic();
                //}

                return pic;
            }
            set
            {

                SetProperty(ref pic, value);
            }
        }

        public string MvUrl { get => mvUrl; set => SetProperty(ref mvUrl, value); }

        public string TypeName { get => typeName; set => SetProperty(ref typeName, value); }

        /// <summary>
        /// 扩展信息
        /// </summary>
        public string ExtData { get; set; }

        public SongExtModel GetExtInfo()
        {
            if (string.IsNullOrEmpty(ExtData))
            {
                return new SongExtModel();
            }

            return StringUtil.Parse<SongExtModel>(ExtData);
        }

        public RelayCommand<string> OpenImageCmd => new RelayCommand<string>(OpenImage);

        private void OpenImage(string pic)
        {
            if (string.IsNullOrEmpty(pic))
            {
                return;
            }

            var uri = new Uri(pic);

            new ImageBrowser(uri)
            {
                Owner = WindowHelper.GetActiveWindow()
            }.Show();
        }

        private string GetDefaultPic()
        {
            return $"\\Images\\song.png";
        }
    }
}
