﻿using BSKTV.Server.Core;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace BSKTV.Server.ViewModel
{
    public class NavItemViewModel : ObservableObject
    {
        private string _name;
        private string _code;
        private string _imagePath;
        private string _pagePath;
        private Page _content;
        private bool _loaded;

        public string Name { get => _name; set => SetProperty(ref _name, value); }

        public string Code { get => _code; set => SetProperty(ref _code, value); }

        public string ImagePath { get => _imagePath; set => SetProperty(ref _imagePath, value); }

        public string PagePath { get => _pagePath; set => SetProperty(ref _pagePath, value); }

        public Page Content { get => _content; set => SetProperty(ref _content, value); }

        public bool Loaded { get => _loaded; private set => SetProperty(ref _loaded, value); }

        public NavItemViewModel() { }

        public NavItemViewModel(string name, string code, string imagePath, string pagePath)
        {
            Name = name;
            Code = code;
            ImagePath = imagePath;
            PagePath = pagePath;
        }

        public RelayCommand<NavigationEventArgs> LoadCmd => new RelayCommand<NavigationEventArgs>(Load);

        private void Load(NavigationEventArgs e)
        {
            if (_content == null) return;

            if (Loaded)
            {
                if (_content.DataContext is IKTVViewModel model)
                {
                    if (model.NeedRefresh)
                    {
                        model.Refresh();
                    }
                }
            }
            else
            {
                if (_content.DataContext is IKTVViewModel model)
                {
                    model.Init();
                }

                Loaded = true;
            }
        }
    }
}
