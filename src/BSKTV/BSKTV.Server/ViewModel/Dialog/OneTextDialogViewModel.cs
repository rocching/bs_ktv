﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using HandyControl.Tools.Extension;
using System;

namespace BSKTV.Server.ViewModel.Dialog
{
    public class OneTextDialogViewModel : ObservableObject, IDialogResultable<int>
    {
        private int widht;
        private int height;
        private string title;
        private string leftTitle;
        private int result;

        public OneTextDialogViewModel()
        {
            Width = 400;
            height = 300;
        }

        public int Width { get => widht; set => SetProperty(ref widht, value); }

        public int Height { get => height; set => SetProperty(ref height, value); }

        public string Title { get => title; set => SetProperty(ref title, value); }

        public string LeftTitle { get => leftTitle; set => SetProperty(ref leftTitle, value); }

        public int Result { get => result; set => SetProperty(ref result, value); }

        public Action CloseAction { get; set; }

        public RelayCommand SureCmd => new RelayCommand(() => CloseAction?.Invoke());

        public RelayCommand CancelCmd => new RelayCommand(() =>
        {
            Result = 0;
            CloseAction?.Invoke();
        });
    }
}
