﻿using BSKTV.Model;
using CommunityToolkit.Mvvm.ComponentModel;
using System;

namespace BSKTV.Server.ViewModel.Server
{
    public class ClientViewModel : ObservableObject, IPrimaryKey
    {
        private long id;
        private string sessionID;
        private string ipAddress;
        private int port;
        private bool connected;
        private string connectedString;
        private DateTime startTime;
        private DateTime lastActiveTime;

        public string SessionID { get => sessionID; set => SetProperty(ref sessionID, value); }

        public string IpAddress { get => ipAddress; set => SetProperty(ref ipAddress, value); }

        public int Port { get => port; set => SetProperty(ref port, value); }

        public bool Connected
        {
            get => connected;
            set
            {
                SetProperty(ref connected, value);

                ConnectedString = connected ? "已连接" : "已断开";
            }
        }

        public string ConnectedString { get => connectedString; set => SetProperty(ref connectedString, value); }

        public DateTime StartTime
        {
            get => startTime; set => SetProperty(ref startTime, value);
        }

        public DateTime LastActiveTime { get => lastActiveTime; set => SetProperty(ref lastActiveTime, value); }
        public long Id { get => id; set => SetProperty(ref id, value); }
    }
}
