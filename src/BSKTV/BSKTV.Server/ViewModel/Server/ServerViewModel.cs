﻿using BSKTV.Model;
using BSKTV.Server.Server;
using BSKTV.Server.Util;
using CommunityToolkit.Mvvm.Input;
using SuperSocket.SocketBase;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace BSKTV.Server.ViewModel.Server
{
    public class ServerViewModel : DefaultPageViewModel<ClientViewModel>
    {
        public ServerViewModel() { }

        private Timer timer;
        private List<string> ipList;
        private string ipAddress;
        private int port;

        private bool startEnabled;
        private ServerState serverStatus;
        private string statusName;

        public string IpAddress { get => ipAddress; set => SetProperty(ref ipAddress, value); }

        public int Port { get => port; set => SetProperty(ref port, value); }

        public bool StartEnabled { get => startEnabled; set => SetProperty(ref startEnabled, value); }

        public ServerState Status { get => serverStatus; set => SetProperty(ref serverStatus, value); }

        public string StatusName { get => statusName; set => SetProperty(ref statusName, value); }

        public List<string> IpList { get => ipList; set => SetProperty(ref ipList, value); }

        public RelayCommand StartCmd => new RelayCommand(Start);

        public RelayCommand StopCmd => new RelayCommand(Stop);

        public override void Init()
        {
            IpList = AppUtil.GetIpList();
            Port = Constant.DEFAULT_SERVER_PORT;

            SetStatus();

            InitTimer();
        }

        public override void LoadData(int pageIndex)
        {
            TotalPage = 1;

            DataList = KtvServerMgr.GetAllClientList();
        }

        private void Start()
        {
            StartEnabled = false;

            KtvServerMgr.Start(IpAddress, Port);
        }

        private void Stop()
        {
            StartEnabled = true;

            KtvServerMgr.Stop();
        }

        private void InitTimer()
        {
            timer = new Timer
            {
                Interval = 5000,
                Enabled = true
            };

            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            SetStatus();

            LoadData(1);
        }

        private void SetStatus()
        {
            Status = KtvServerMgr.GetServerState();
            StatusName = KtvServerMgr.GetServerStateString();

            if (Status == ServerState.NotStarted)
            {
                StartEnabled = true;
            }
            else
            {
                StartEnabled = false;
            }
        }
    }
}
