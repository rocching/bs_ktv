﻿using BSKTV.Model;
using CommunityToolkit.Mvvm.ComponentModel;

namespace BSKTV.Server.ViewModel.Dictionary
{
    public class DictionaryViewModel : ObservableObject, IPrimaryKey
    {
        private long id;
        private string type;
        private string code;
        private string name;
        private int sortId;

        public long Id { get => id; set => SetProperty(ref id, value); }
        public string Type { get => type; set => SetProperty(ref type, value); }
        public string Code { get => code; set => SetProperty(ref code, value); }
        public string Name { get => name; set => SetProperty(ref name, value); }
        public int SortId { get => sortId; set => SetProperty(ref sortId, value); }
    }
}
