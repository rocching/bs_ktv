﻿using BSKTV.Model;
using BSKTV.Model.Input;
using BSKTV.Server.ViewModel.Song;
using BSKTV.Service;
using BSKTV.Util;
using CommunityToolkit.Mvvm.Input;
using HandyControl.Controls;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace BSKTV.Server.ViewModel.Dictionary
{
    public class DictionaryListViewModel : DefaultPageViewModel<DictionaryViewModel>
    {
        private readonly DictionaryService dictionaryService;

        public DictionaryListViewModel(DictionaryService dictionaryService)
        {
            this.dictionaryService = dictionaryService;

            Type = Constant.DICTIONARY_TYPE_DEFAULT;
        }

        private string type;

        public string Type { get => type; set => SetProperty(ref type, value); }

        private ObservableCollection<DictionaryModel> _typeList;
        public ObservableCollection<DictionaryModel> TypeList { get => _typeList; protected set => SetProperty(ref _typeList, value); }

        public RelayCommand<DataGridRowEditEndingEventArgs> RowEditEndingCmd => new RelayCommand<DataGridRowEditEndingEventArgs>(RowEditEnding);

        public RelayCommand AddCmd => new RelayCommand(Add);

        public RelayCommand DeleteCmd => new RelayCommand(Delete);

        public override async void Init()
        {
            await LoadTypeData();

            PageSize = 10;
            PageIndex = 1;
        }

        public override async void LoadData(int pageIndex)
        {
            GetDictionaryPagedListInput input = new GetDictionaryPagedListInput
            {
                PageIndex = pageIndex,
                PageSize = PageSize,
                Type = Type,
                Keyword = Keyword
            };

            var pagedModel = await dictionaryService.GetPagedList(input);

            TotalPage = pagedModel.GetTotalPage();
            DataList = StringUtil.CopyList<DictionaryModel, DictionaryViewModel>(pagedModel.List);
        }

        public async Task LoadTypeData()
        {
            var typeList = await dictionaryService.GetList(Constant.DICTIONARY_TYPE);

            TypeList = StringUtil.CopyList(typeList);
        }

        private void Add()
        {
            var model = new DictionaryViewModel
            {
                SortId = 100
            };

            DataList.Insert(0, model);
        }

        private async void Delete()
        {
            if (HasSelectedItem())
            {
                var idList = GetSelectedIdList();
                bool flag = await dictionaryService.Delete(idList);
                if (flag)
                {
                    Growl.Success("字典删除成功!");

                    ClearIdList();

                    Refresh();
                }
                else
                {
                    Growl.Error("字典删除失败!");
                }
            }
            else
            {
                Growl.Warning("还没有选择任何字典项!");
            }
        }

        private async void RowEditEnding(DataGridRowEditEndingEventArgs args)
        {
            if (args.EditAction == DataGridEditAction.Commit)
            {
                if (args.Row.DataContext is DictionaryViewModel vm)
                {
                    var model = StringUtil.Copy<DictionaryViewModel, DictionaryModel>(vm);

                    var newModel = await dictionaryService.Save(model);

                    if (model.Id > 0)
                    {
                        Growl.Success("修改字典成功!");
                    }
                    else
                    {
                        vm.Id = newModel.Id;

                        Growl.Success("新增字典成功!");
                    }
                }
            }
        }
    }
}
