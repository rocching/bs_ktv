﻿using BSKTV.Model;
using BSKTV.Server.Core;
using BSKTV.Util;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using HandyControl.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace BSKTV.Server.ViewModel
{
    public class DefaultPageViewModel<TModel> : ObservableObject, IKTVViewModel where TModel : class, IPrimaryKey
    {
        public DefaultPageViewModel()
        {
            selectedList = new List<TModel>();
        }

        public virtual void Init()
        {
            PageSize = 10;
            PageIndex = 1;
        }

        public virtual void Refresh()
        {
            PageIndex = _pageIndex;
        }

        public virtual void LoadData(int pageIndex)
        {

        }

        private readonly List<TModel> selectedList;
        private bool _needRefresh;
        private int _pageIndex;
        private int _pageSize;
        private long _totalPage;

        public virtual bool NeedRefresh { get => _needRefresh; set => _needRefresh = value; }

        public virtual int PageIndex
        {
            get => _pageIndex;
            set
            {
                SetProperty(ref _pageIndex, value);

                LoadData(_pageIndex);
            }
        }

        public virtual int PageSize
        {
            get => _pageSize;
            set
            {
                SetProperty(ref _pageSize, value);
            }
        }

        public long TotalPage { get => _totalPage; protected set => SetProperty(ref _totalPage, value); }

        private string _keyword;
        public string Keyword { get => _keyword; set => SetProperty(ref _keyword, value); }

        private ObservableCollection<TModel> dataList;

        public ObservableCollection<TModel> DataList { get => dataList; protected set => SetProperty(ref dataList, value); }

        public RelayCommand SearchCmd => new RelayCommand(Search);

        public RelayCommand<KeyEventArgs> KeywordKeyUpCmd => new RelayCommand<KeyEventArgs>(KeywordKeyUp);

        public RelayCommand<SelectionChangedEventArgs> GridSelectionChangedCmd => new RelayCommand<SelectionChangedEventArgs>(GridSelectionChanged);

        public void Do(Action action)
        {
            Application.Current.Dispatcher?.Invoke(action);
        }

        public virtual void Search()
        {
            PageIndex = 1;
        }

        public virtual void KeywordKeyUp(KeyEventArgs args)
        {
            if (args.Key == Key.Enter)
            {
                if (string.IsNullOrEmpty(Keyword))
                {
                    Growl.Warning("请输入关键词!");
                    return;
                }

                PageIndex = 1;
            }
        }

        public bool HasData()
        {
            return dataList.Count > 0;
        }

        protected void AddItem(TModel model)
        {
            if (model == null)
            {
                return;
            }

            selectedList.Add(model);
        }

        protected bool HasSelectedItem()
        {
            return selectedList.Count > 0;
        }

        protected virtual List<long> GetSelectedIdList()
        {
            return selectedList.Select(m => m.Id).ToList();
        }

        protected List<TModel> GetSelectedList()
        {
            return selectedList;
        }

        public TModel GetSelectedFirst()
        {
            var list = GetSelectedList();

            if (list != null && list.Count > 0)
            {
                return list[0];
            }

            return null;
        }

        public TModel GetItemByIndex(int index)
        {
            if (index >= 0 && index < dataList.Count)
            {
                return dataList[index];
            }

            return dataList[0];
        }

        public int GetItemIndex(TModel model)
        {
            return dataList.IndexOf(model);
        }

        protected void ClearIdList()
        {
            selectedList.Clear();
        }

        protected virtual void GridSelectionChanged(SelectionChangedEventArgs args)
        {
            selectedList.Clear();

            if (args.Source is DataGrid grid)
            {
                foreach (var item in grid.SelectedItems)
                {
                    if (item is TModel model)
                    {
                        selectedList.Add(StringUtil.Copy(model));
                    }
                }
            }
        }
    }
}
