﻿using BSKTV.Model;
using BSKTV.Server.Schedule;
using BSKTV.Util;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using HandyControl.Controls;
using System;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace BSKTV.Server.ViewModel
{
    public class MainViewModel : ObservableObject
    {
        private readonly PageService pageService;
        private readonly MyScheduler scheduler;

        public MainViewModel(PageService pageService, MyScheduler scheduler)
        {
            this.pageService = pageService;
            this.scheduler = scheduler;
        }

        public void Load()
        {
            Init();

            InitNavigation();

            InitTask();
        }


        private string _title;

        public string Title { get => _title; set => SetProperty(ref _title, value); }

        public MainWindow MainWindow { get; set; }

        private void Init()
        {
            Title = GetAppTitle();
        }

        private string GetAppTitle()
        {
            return $"{Constant.AppName} {Constant.AppVersion}";
        }

        #region 定时任务

        private void InitTask()
        {
            scheduler.Run();
        }

        #endregion

        #region 导航

        private int _tabCurrentIndex;

        public int TabCurrentIndex { get => _tabCurrentIndex; set => SetProperty(ref _tabCurrentIndex, value); }

        private NavItemViewModel _currentNavItem;

        public NavItemViewModel CurrentNavItem { get => _currentNavItem; set => SetProperty(ref _currentNavItem, value); }

        private ObservableCollection<NavItemViewModel> _menuItems;
        public ObservableCollection<NavItemViewModel> MenuItemList
        {
            get => _menuItems;
            set => SetProperty(ref _menuItems, value);
        }

        private ObservableCollection<NavItemViewModel> _tabMenuItems;

        public ObservableCollection<NavItemViewModel> TabMenuItemList
        {
            get => _tabMenuItems;
            set => SetProperty(ref _tabMenuItems, value);
        }

        //public RelayCommand<FunctionEventArgs<object>> SwitchMenuCmd => new RelayCommand<FunctionEventArgs<object>>(SwitchMenu);

        public RelayCommand<SelectionChangedEventArgs> SwitchTabCmd => new RelayCommand<SelectionChangedEventArgs>(SwitchTab);

        public RelayCommand<string> MenuClickCmd => new RelayCommand<string>(MenuClick);

        private void InitNavigation()
        {
            TabMenuItemList = new ObservableCollection<NavItemViewModel>();
            MenuItemList = new ObservableCollection<NavItemViewModel>
            {
                GetNavItemViewModel(Constant.App.HomePage, "首页", @"Images\home.png",@"View\Page\HomePage.xaml"),
                GetNavItemViewModel(Constant.App.DictionaryPage,"字典管理",@"Images\dictionary.png",@"View\Page\Dictionary\DictionaryListPage.xaml"),
                GetNavItemViewModel(Constant.App.ServerPage,"服务管理",@"Images\server.png",@"View\Page\Server\ServerPage.xaml"),
                GetNavItemViewModel(Constant.App.SingerPage, "歌手管理", @"Images\singer.png",@"View\Page\Singer\SingerListPage.xaml"),
                GetNavItemViewModel(Constant.App.KugouSingerPage, "酷狗歌手", @"Images\kugou.png", @"View\Page\Singer\KgSingerListPage.xaml"),
                GetNavItemViewModel(Constant.App.SongPage,"歌曲管理",@"Images\song_list.png",@"View\Page\Song\SongListPage.xaml"),
                GetNavItemViewModel(Constant.App.Net91SongPage,"[91]歌曲管理",@"Images\song_list91.png",@"View\Page\Song\Net91SongListPage.xaml"),
                GetNavItemViewModel(Constant.App.PlayListPage,"播放列表",@"Images\play_list.png",@"View\Page\Song\PlayListPage.xaml")
            };

            SwitchTabPage(_menuItems[0]);
        }

        private void SwitchTab(SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count < 1)
            {
                if (e.Source is HandyControl.Controls.TabControl tab)
                {
                    if (tab.Items.Count < 1)
                    {
                        SetSideMenuSelected("");
                    }
                }
                return;
            }

            var item = e.AddedItems[0];

            if (item is NavItemViewModel model)
            {
                CurrentNavItem = model;

                SetSideMenuSelected(model.Code);
            }
        }

        private void MenuClick(string code)
        {
            if (string.IsNullOrEmpty(code)) return;

            var itemModel = GetNavItemViewModel(code);

            if (itemModel == null) return;

            SwitchTabPage(itemModel);
        }

        private NavItemViewModel GetNavItemViewModel(string code, string name, string imagePath, string pagePath)
        {
            //var imageBrush = ResourceHelper.GetResource<object>(imagePath);

            return new NavItemViewModel(name, code, imagePath, pagePath);
        }

        //private void SwitchMenu(FunctionEventArgs<object> args)
        //{
        //    if (args.Info == null) return;

        //    if (args.Info is SideMenuItem menu)
        //    {
        //        var itemModel = GetNavItemViewModel(menu.CommandParameter.ToString());

        //        if (itemModel == null) return;

        //        //SetSideMenuSelected(itemModel.Code);

        //        SwitchTabPage(itemModel);
        //    }
        //}

        private void SetSideMenuSelected(string code)
        {
            var root = MainWindow.RootSideMenu.Items[0];

            if (root is SideMenuItem itemMenu)
            {
                foreach (var item in itemMenu.Items)
                {
                    if (item is SideMenuItem subItemMenu)
                    {
                        subItemMenu.IsSelected = false;

                        string p = subItemMenu.CommandParameter.ToString();

                        if (p == code)
                        {
                            subItemMenu.IsSelected = true;
                        }
                    }
                }
            }
        }

        private void SwitchTabPage(NavItemViewModel viewModel)
        {
            var item = _tabMenuItems.FirstOrDefault(m => m.Code == viewModel.Code);
            if (item != null)
            {
                CurrentNavItem = item;

                TabCurrentIndex = _tabMenuItems.IndexOf(item);
            }
            else
            {
                var page = pageService.CreatePage(viewModel.Code);

                if (page == null) return;

                if (page is Page content)
                {
                    viewModel.Content = content;

                    CurrentNavItem = viewModel;

                    TabMenuItemList.Add(viewModel);

                    TabCurrentIndex = TabMenuItemList.Count - 1;
                }
            }
        }

        private NavItemViewModel GetNavItemViewModel(string code)
        {
            return _menuItems.FirstOrDefault(m => m.Code == code);
        }
        #endregion

        #region 操作

        private bool shutDownApp = false;

        public bool ShutDownApp { get => shutDownApp; set => shutDownApp = value; }

        public RelayCommand ExitCmd => new RelayCommand(Exit);

        private void Exit()
        {
            ShutDownApp = true;

            Environment.Exit(0);
        }

        #endregion
    }
}
