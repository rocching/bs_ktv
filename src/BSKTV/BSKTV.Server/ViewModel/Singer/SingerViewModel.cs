﻿using BSKTV.Model;
using CommunityToolkit.Mvvm.ComponentModel;

namespace BSKTV.Server.ViewModel.Singer
{
    public class SingerViewModel : ObservableObject, IPrimaryKey
    {
        public SingerViewModel() { }

        private long _id;
        private string _sexType;
        private string _pic;
        private string _singerId;
        private string _singerName;
        private int _fansCount;
        private string _singerNameSzm;

        public long Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }

        public string SexType { get => _sexType; set => SetProperty(ref _sexType, value); }
        public string Pic
        {
            get => _pic;
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    value = GetDefaultPic();
                }

                SetProperty(ref _pic, value);
            }
        }
        public string SingerId { get => _singerId; set => SetProperty(ref _singerId, value); }
        public string SingerName { get => _singerName; set => SetProperty(ref _singerName, value); }

        public int FansCount { get => _fansCount; set => SetProperty(ref _fansCount, value); }
        public string SingerNameSzm { get => _singerNameSzm; set => SetProperty(ref _singerNameSzm, value); }

        private string GetDefaultPic()
        {
            string name = "";

            switch (SexType)
            {
                case Constant.SINGER_SEX_TYPE_MALE:
                    name = "singer_male";
                    break;
                case Constant.SINGER_SEX_TYPE_FEMALE:
                    name = "singer_female";
                    break;
                case Constant.SINGER_SEX_TYPE_GROUP:
                    name = "singer_group";
                    break;
            }

            return $"Images\\{name}.png";
        }
    }
}
