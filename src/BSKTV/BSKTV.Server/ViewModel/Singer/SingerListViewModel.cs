﻿using BSKTV.Model;
using BSKTV.Model.Input;
using BSKTV.Server.Core;
using BSKTV.Service;
using BSKTV.Util;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using HandyControl.Controls;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace BSKTV.Server.ViewModel.Singer
{
    public class SingerListViewModel : DefaultPageViewModel<SingerViewModel>
    {
        private readonly DictionaryService dictionaryService;
        private readonly SingerService singerService;
        public SingerListViewModel(DictionaryService dictionaryService, SingerService singerService)
        {
            this.dictionaryService = dictionaryService;
            this.singerService = singerService;

            Type = Constant.SINGER_TYPE_DEFAULT_VALUE;
            SexType = Constant.SINGER_SEX_TYPE_DEFAULT_VALUE;

            PageSize = 10;
        }

        public override async void Init()
        {
            await LoadTypeData();

            PageIndex = 1;
        }

        private ObservableCollection<DictionaryModel> _sexTypeList;
        public ObservableCollection<DictionaryModel> SexTypeList { get => _sexTypeList; protected set => SetProperty(ref _sexTypeList, value); }

        private ObservableCollection<DictionaryModel> _typeList;
        public ObservableCollection<DictionaryModel> TypeList { get => _typeList; protected set => SetProperty(ref _typeList, value); }

        private string _type;
        private string _sexType;

        public string Type { get => _type; set => SetProperty(ref _type, value); }
        public string SexType { get => _sexType; set => SetProperty(ref _sexType, value); }

        public RelayCommand<DataGridCellEditEndingEventArgs> CellEditEndingCmd => new RelayCommand<DataGridCellEditEndingEventArgs>(CellEditEnding);

        public virtual async Task LoadTypeData()
        {
            var sexTypeList = await dictionaryService.GetList(Constant.SINGER_SEX_TYPE);
            var typeList = await dictionaryService.GetList(Constant.SINGER_TYPE);

            SexTypeList = StringUtil.CopyList(sexTypeList);
            TypeList = StringUtil.CopyList(typeList);
        }

        public override async void LoadData(int pageIndex)
        {
            GetSingerPagedListInput input = new GetSingerPagedListInput
            {
                Type = Type,
                SexType = SexType,
                Keyword = Keyword,
                PageIndex = pageIndex,
                PageSize = PageSize
            };

            var pagedModel = await singerService.GetPagedList(input);

            TotalPage = pagedModel.GetTotalPage();
            DataList = StringUtil.CopyList<SingerModel, SingerViewModel>(pagedModel.List);
        }

        private async void CellEditEnding(DataGridCellEditEndingEventArgs e)
        {
            if (e.EditAction == DataGridEditAction.Commit)
            {
                if (e.Row.DataContext is SingerViewModel model)
                {
                    string text = "";
                    if (e.EditingElement is System.Windows.Controls.TextBox box)
                    {
                        text = box.Text;
                    }

                    string columnName = e.Column.SortMemberPath;

                    if (columnName == Constant.SINGER_FIELD_SINGER_NAME)
                    {
                        if (string.IsNullOrEmpty(text))
                        {
                            Growl.Warning("歌手名称不能为空!");
                            e.Cancel = true;
                            return;
                        }

                        if (text == model.SingerName)
                        {
                            return;
                        }

                        var updateModel = await singerService.UpdateSingerName(model.Id, text);
                        if (updateModel == null)
                        {
                            Growl.Error("修改歌手名称失败!");
                        }
                        else
                        {
                            model.SingerName = updateModel.SingerName;
                            model.SingerNameSzm = updateModel.SingerNameSzm;

                            Growl.Success("修改歌手名称成功!");
                        }
                    }
                }
            }
        }
    }
}
