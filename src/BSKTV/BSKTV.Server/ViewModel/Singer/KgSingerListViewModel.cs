﻿using BSKTV.Model;
using BSKTV.Model.Input;
using BSKTV.Service;
using BSKTV.Util;
using CommunityToolkit.Mvvm.Input;
using HandyControl.Controls;

namespace BSKTV.Server.ViewModel.Singer
{
    public class KgSingerListViewModel : SingerListViewModel
    {
        private readonly KugouService kugouService;
        private readonly SingerService singerService;
        public KgSingerListViewModel(DictionaryService dictionaryService, SingerService singerService, KugouService kugouService) : base(dictionaryService, singerService)
        {
            this.kugouService = kugouService;
            this.singerService = singerService;

            PageSize = 20;
        }

        public override async void LoadData(int pageIndex)
        {
            GetSingerPagedListInput input = new GetSingerPagedListInput
            {
                Type = Type,
                SexType = SexType,
                Keyword = Keyword,
                PageIndex = pageIndex,
                PageSize = PageSize
            };

            var pagedModel = await kugouService.GetPagedList(input);

            TotalPage = pagedModel.GetTotalPage();
            DataList = StringUtil.CopyList<SingerModel, SingerViewModel>(pagedModel.List);
        }

        public RelayCommand AddCmd => new RelayCommand(Add);

        private async void Add()
        {
            if (DataList == null || DataList.Count < 1)
            {
                Growl.Warning("请先查询酷狗歌手列表!");
            }

            var list = StringUtil.CopyList<SingerViewModel, SingerModel>(DataList);

            bool flag = await singerService.AddList(list);

            if (flag)
            {
                Growl.Success("添加酷狗歌手成功!");
            }
            else
            {
                Growl.Error("添加酷狗歌手失败!");
            }
        }

        private async void AddAll()
        {
            int pageCount = 101;

            for (int i = 1; i <= pageCount; i++)
            {
                GetSingerPagedListInput input = new GetSingerPagedListInput
                {
                    Type = "1",
                    SexType = "3",
                    Keyword = "",
                    PageIndex = i,
                    PageSize = 20
                };

                var pagedModel = await kugouService.GetPagedList(input);

                long totalCount = pagedModel.TotalCount;

                if (totalCount > 0)
                {
                    bool flag = await singerService.AddList(pagedModel.List);

                    if (flag)
                    {
                        Growl.Success($"[{i}]添加酷狗歌手成功!");
                    }
                    else
                    {
                        Growl.Error($"[{i}]添加酷狗歌手失败!");
                    }
                }
                else
                {
                    LogUtil.Instance.WriteLog($"找不到到更多的数据-{i}");
                    break;
                }
            }
        }
    }
}
