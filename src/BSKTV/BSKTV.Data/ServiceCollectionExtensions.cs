﻿using Microsoft.Extensions.DependencyInjection;

namespace BSKTV.Data
{
    public static class ServiceCollectionExtensions
    {
        public static void AddKTVRepository(this IServiceCollection services)
        {
            services.AddScoped<DictionaryRepository>();
            services.AddScoped<SingerRepository>();
            services.AddScoped<SongRepository>();
            services.AddScoped<PlayListRepository>();
        }
    }
}
