﻿using BSKTV.Model;
using BSKTV.Model.Input;
using BSKTV.Util;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BSKTV.Data
{
    public class PlayListRepository : BaseRepository<PlayListModel>
    {
        public PlayListRepository() { }

        public async Task<PagedModel<SongPlayListModel>> GetPagedList(GetSongPlayListPagedListInput input)
        {
            var where2 = ExpressionUtil.Default<SongModel>();

            int type = input.Type;
            string source = input.Source;
            string keyword = input.Keyword;
            string clientId = input.ClientId;
            string userName = input.UserName;
            string keywordUpper = "";

            if (!string.IsNullOrEmpty(keyword))
            {
                keywordUpper = keyword.ToUpper();
            }

            where2 = where2.MergeAnd(type > 0, m => m.Type == type);
            where2 = where2.MergeAnd(!string.IsNullOrEmpty(source), m => m.Source == source);
            where2 = where2.MergeAnd(!string.IsNullOrEmpty(keyword), m => m.SongId == keyword || m.SongNameSzm == keywordUpper || m.SingerName.Contains(keyword) || m.SongName.Contains(keyword));

            var where = ExpressionUtil.Default<PlayListModel>();
            where = where.MergeAnd(!string.IsNullOrEmpty(clientId), m => m.ClientId == clientId);
            where = where.MergeAnd(!string.IsNullOrEmpty(userName), m => m.UserName == userName);

            var sql = GetSqlLam().Select(m => new { PlayId = m.Id, m.ClientId, m.UserName, m.SortId }).Where(where).OrderBy(m => m.SortId).SqlClear(false);

            sql.Join<SongModel>((p, s) => p.SongId == s.Id, Roc.Data.SqlJoinType.INNER, "b").SelectAll().Where(where2);

            var list = DbClient.GetList<SongPlayListModel>(sql.GetSql(), sql.GetParameters());

            List<SongPlayListModel> playList = list == null ? new List<SongPlayListModel>() : list.ToList();

            sql.Count(m => m.Id);

            var totalCount = await GetCountAsync(sql);

            sql.Clear();

            return new PagedModel<SongPlayListModel>(input, totalCount, playList);
        }
    }
}
