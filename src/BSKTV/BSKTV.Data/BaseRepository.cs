﻿using BSKTV.Model;
using BSKTV.Util;
using Roc.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BSKTV.Data
{
    public abstract class BaseRepository<T> where T : class, IPrimaryKey
    {
        private const ProviderType type = ProviderType.SQLite;
        private const string aliasName = "a";

        protected virtual DbClient DbClient { get; private set; }

        public BaseRepository()
        {
            InitClient();
        }

        private void InitClient()
        {
            DbClient = new DbClient(type)
            {
                Log = WriteLog,
                Error = WriteLog
            };
        }

        public virtual SqlLam<T> GetSqlLam()
        {
            return new SqlLam<T>(aliasName, type);
        }

        #region 日志

        private void WriteLog(SqlLogArgs sql)
        {
            int type = sql.Type;

            if (type == 1)
            {
                WriteLog("入库之前:");
                WriteLog("SQL语句:");
                WriteLog(sql.Text);
                WriteLog("参数列表:");

                List<string> argsList = new List<string>();

                var ps = sql.Parameters;
                if (ps != null && ps.Count > 0)
                {
                    argsList.Add("");
                    foreach (var item in ps)
                    {
                        argsList.Add($"[{item.Name}]/[{item.Value}]");
                    }

                    WriteLog(string.Join("\r\n", argsList));
                }
            }
            else if (type == 3)
            {
                var e = sql.Error; if (e != null)
                {
                    WriteLog($"发生错误:{e.Message}");
                    WriteLog("详细信息:\r\n" + e.ToString());
                }
            }
        }

        private void WriteLog(string msg)
        {
            LogUtil.Instance.WriteLog(msg);
        }

        #endregion

        #region 查询

        public virtual async Task<TModel> GetAsync<TModel>(string sql, Dictionary<string, object> parameters = null)
        {
            var list = await GetListAsync<TModel>(sql, parameters);

            if (list != null && list.Count > 0)
            {
                return list[0];
            }

            return default;
        }

        public virtual async Task<List<TModel>> GetListAsync<TModel>(string sql, Dictionary<string, object> parameters = null)
        {
            return await Task.Run(() =>
            {
                var list = DbClient.GetList<TModel>(sql, parameters);

                if (list != null)
                {
                    return list.ToList();
                }

                return null;
            });
        }

        public virtual async Task<List<T>> GetListAsync(SqlLamBase sql)
        {
            return await Task.Run(() =>
             {
                 var list = DbClient.GetList<T>(sql.GetSql(), sql.GetParameters());

                 if (list != null)
                 {
                     return list.ToList();
                 }

                 return null;
             });
        }

        public virtual List<T> GetList(SqlLamBase sql)
        {
            var list = DbClient.GetList<T>(sql.GetSql(), sql.GetParameters());

            if (list != null)
            {
                return list.ToList();
            }

            return null;
        }

        public virtual async Task<List<T>> GetList(Expression<Func<T, bool>> expression)
        {
            var sql = GetSqlLam().Where(expression);

            return await GetListAsync(sql);
        }

        public virtual async Task<List<T>> GetList(string where, Dictionary<string, object> parameters = null)
        {
            var sql = GetSqlLam().Where(where);

            if (parameters != null && parameters.Count > 0)
            {
                foreach (var p in parameters)
                {
                    sql.AddParameter(p.Key, p.Value);
                }
            }

            return await GetListAsync(sql);
        }

        public virtual async Task<List<T>> GetList<TKey>(Expression<Func<T, bool>> where, params Expression<Func<T, TKey>>[] orderByList)
        {
            return await GetList(where, isAsc: true, orderByList);
        }

        public virtual async Task<List<T>> GetList<TKey>(Expression<Func<T, bool>> where, bool isAsc = true, params Expression<Func<T, TKey>>[] orderByList)
        {
            var sql = GetSqlLam().Where(where);

            if (isAsc)
            {
                sql.OrderBy(orderByList);
            }
            else
            {
                sql.OrderByDescending(orderByList);
            }

            return await GetListAsync(sql);
        }

        public virtual async Task<T> Get(object key)
        {
            return await Task.Run(() =>
            {
                var sql = GetSqlLam().Key(key);

                return DbClient.Get<T>(sql.GetSql(), sql.GetParameters());
            });
        }

        public virtual async Task<T> Get(Expression<Func<T, bool>> where)
        {
            return await Task.Run(() =>
            {
                var sql = GetSqlLam().Where(where);

                return DbClient.Get<T>(sql.GetSql(), sql.GetParameters());
            });
        }

        public virtual async Task<long> GetCountAsync(Expression<Func<T, bool>> where, Expression<Func<T, object>> field)
        {
            var sql = GetSqlLam().Where(where).Count(field);

            return await GetCountAsync(sql);
        }

        public virtual long GetCount(Expression<Func<T, bool>> where, Expression<Func<T, object>> field)
        {
            var sql = GetSqlLam().Where(where).Count(field);

            return GetCount(sql);
        }

        public virtual async Task<long> GetCountAsync(Expression<Func<T, bool>> where, string name)
        {
            var sql = GetSqlLam().Where(where).Count(name);

            return await GetCountAsync(sql);
        }

        public virtual long GetCount(Expression<Func<T, bool>> where, string name)
        {
            var sql = GetSqlLam().Where(where).Count(name);

            return GetCount(sql);
        }

        public virtual async Task<long> GetCountAsync(Expression<Func<T, bool>> where)
        {
            return await GetCountAsync(where, "0");
        }

        public virtual long GetCount(Expression<Func<T, bool>> where)
        {
            return GetCount(where, "0");
        }

        public virtual async Task<long> GetCountAsync(SqlLamBase sql)
        {
            return await Task.Run(() => GetCount(sql));
        }

        public virtual long GetCount(SqlLamBase sql)
        {
            return DbClient.ExecuteScalar<long>(sql.GetSql(), sql.GetParameters());
        }

        public virtual async Task<bool> ExistAsync(Expression<Func<T, bool>> where, Expression<Func<T, object>> field)
        {
            long count = await GetCountAsync(where, field);

            return count > 0;
        }

        public virtual async Task<bool> ExistAsync(Expression<Func<T, bool>> where, string name)
        {
            long count = await GetCountAsync(where, name);

            return count > 0;
        }

        #endregion

        #region 分页

        public virtual async Task<PagedModel<T>> GetPagedList(PageInput input, Expression<Func<T, bool>> where, string orderByString)
        {
            return await Task.Run(() =>
            {
                var sql = GetSqlLam().Where(where).OrderBy(orderByString).QueryPage(input.PageIndex, input.PageSize);

                var list = GetList(sql);

                var totalCount = GetCount(where);

                return new PagedModel<T>(input, totalCount, list);
            });
        }

        public virtual async Task<PagedModel<T>> GetPagedList<TKey>(PageInput input, Expression<Func<T, bool>> where, Expression<Func<T, TKey>> orderBy, bool isAsc = true)
        {
            return await Task.Run(() =>
            {
                var sql = GetSqlLam().Where(where);
                if (isAsc)
                {
                    sql.OrderBy(orderBy);
                }
                else
                {
                    sql.OrderByDescending(orderBy);
                }

                sql.QueryPage(input.PageIndex, input.PageSize);

                var list = GetList(sql);

                var totalCount = GetCount(where);

                return new PagedModel<T>(input, totalCount, list);
            });
        }

        public virtual async Task<PagedModel<T>> GetPagedList(PageInput input, SqlLamBase sql, SqlLamBase sqlCount)
        {
            var list = await GetListAsync(sql);

            var totalCount = await GetCountAsync(sqlCount);

            return new PagedModel<T>(input, totalCount, list);
        }

        #endregion

        #region 插入

        public virtual bool Insert(T model)
        {
            var sql = GetSqlLam().Insert(model, increment: true);

            int count = DbClient.ExecuteNonQuery(sql.GetSql(), sql.GetParameters());

            return count > 0;
        }

        public virtual async Task<bool> InsertAsync(T model)
        {
            return await Task.Run(() => Insert(model));
        }

        public virtual int InsertWithId(T model)
        {
            var sql = GetSqlLam().Insert(model, increment: true);

            return DbClient.ExecuteScalar<int>(sql.GetSql(), sql.GetParameters());
        }

        public virtual async Task<int> InsertWithIdAsync(T model)
        {
            return await Task.Run(() => InsertWithId(model));
        }

        public virtual async Task<bool> InsertListAsync(IEnumerable<T> list)
        {
            return await Task.Run(() =>
            {
                var sql = GetSqlLam().Insert(list);

                int count = DbClient.ExecuteNonQuery(sql.GetSql(), sql.GetParameters());

                return count > 0;
            });
        }

        public virtual bool InsertList(IEnumerable<T> list)
        {
            var sql = GetSqlLam().Insert(list);

            int count = DbClient.ExecuteNonQuery(sql.GetSql(), sql.GetParameters());

            return count > 0;
        }

        #endregion

        #region 修改

        public virtual bool UpdateById(T model)
        {
            var sql = GetSqlLam().Where(m => m.Id == model.Id).Update(model, ignoreKey: true);

            int count = DbClient.ExecuteNonQuery(sql.GetSql(), sql.GetParameters());

            return count > 0;
        }

        public virtual bool Update(T model, Expression<Func<T, bool>> where)
        {
            var sql = GetSqlLam().Where(where).Update(model, ignoreKey: true);

            int count = DbClient.ExecuteNonQuery(sql.GetSql(), sql.GetParameters());

            return count > 0;
        }

        public virtual bool Update(T model, string where)
        {
            var sql = GetSqlLam().Where(where).Update(model, ignoreKey: true);

            int count = DbClient.ExecuteNonQuery(sql.GetSql(), sql.GetParameters());

            return count > 0;
        }

        public virtual async Task<bool> UpdateAsync(T model, Expression<Func<T, bool>> where)
        {
            return await Task.Run(() => Update(model, where));
        }

        public virtual bool Update(T model, Expression<Func<T, bool>> where, Expression<Func<T, object>> fields)
        {
            var sql = GetSqlLam().Where(where).Update(model, fields);
            int count = DbClient.ExecuteNonQuery(sql.GetSql(), sql.GetParameters());
            return count > 0;
        }

        public virtual bool Update(T model, string where, Expression<Func<T, object>> fields)
        {
            var sql = GetSqlLam().Where(where).Update(model, fields);
            int count = DbClient.ExecuteNonQuery(sql.GetSql(), sql.GetParameters());
            return count > 0;
        }

        public virtual async Task<bool> UpdateAsync(T model, Expression<Func<T, bool>> where, Expression<Func<T, object>> fields)
        {
            return await Task.Run(() => Update(model, where, fields));
        }

        public virtual async Task<bool> UpdateByIdAsync(T model, Expression<Func<T, object>> fields)
        {
            return await Task.Run(() => Update(model, $"Id={model.Id}", fields));
        }

        public virtual async Task<bool> UpdateByIdAsync(T model)
        {
            return await Task.Run(() => Update(model, $"Id={model.Id}"));
        }

        #endregion

        #region 删除

        public virtual bool Delete(Expression<Func<T, bool>> where)
        {
            var sql = GetSqlLam().Delete(where);

            int count = DbClient.ExecuteNonQuery(sql.GetSql(), sql.GetParameters());

            return count > 0;
        }

        public virtual bool DeleteById(long id)
        {
            var sql = GetSqlLam().Where($"Id={id}").Delete();

            int count = DbClient.ExecuteNonQuery(sql.GetSql(), sql.GetParameters());

            return count > 0;
        }

        public virtual async Task<bool> DeleteAsync(Expression<Func<T, bool>> where)
        {
            return await Task.Run(() => Delete(where));
        }

        public virtual async Task<bool> DeleteByIdAsync(long id)
        {
            return await Task.Run(() => DeleteById(id));
        }

        public virtual void Truncate()
        {
            var sql = GetSqlLam();
            sql.Truncate();

            DbClient.ExecuteNonQuery(sql.GetSql(), sql.GetParameters());
        }

        public virtual async Task TruncateAsync()
        {
            await Task.Run(() => Truncate());
        }

        #endregion
    }
}
